<?php
/**
 *  @file
 *  Dynamically generated stylesheet for the live demo (preview)
 */
?>

<style>

  #mopub_demo .demo_topbar { background-color: <?php echo variable_get('mopub_topbar_color', '#5585a0'); ?>; }

  #mopub_demo .demo_menu { background-color: <?php echo variable_get('mopub_menu_bg_color', '#333333'); ?>; }

  #mopub_demo .demo_menu .current a { color: <?php echo variable_get('mopub_menu_active_text_color', '#ffffff'); ?>; }

  #mopub_demo .demo_menu ul li.current a {
    background-color: <?php echo variable_get('mopub_menu_active_bg_color', '#5584a0'); ?>;
  }

  #mopub_demo .demo_menu .inactive a { color: <?php echo variable_get('mopub_menu_text_color', '#9c9c9c'); ?>; }

  #mopub_demo .demo_article { border-color: <?php echo variable_get('mopub_content_border_color', '#c9c9c9'); ?>; }

  #mopub_demo .viewer { background-color: <?php echo variable_get('mopub_content_bg_color', '#ffffff'); ?>; }

  #mopub_demo .demo_article h3 a { color: <?php echo variable_get('mopub_type_title_color', '#365b70'); ?>; }

  #mopub_demo .demo_body .meta { color: <?php echo variable_get('mopub_type_meta_color', '#a3a3a3'); ?>; }

  #mopub_demo .demo_body a { color: <?php echo variable_get('mopub_type_link_color', '#365b70'); ?>; }

  #mopub_demo .demo_body { color: <?php echo variable_get('mopub_type_body_color', '#707070'); ?>; }

  #mopub_demo .viewer {
  <?php if ( variable_get('mopub_type_font')) {
    echo 'font-family: ' . variable_get('mopub_type_font');
  }?>; }

</style>
