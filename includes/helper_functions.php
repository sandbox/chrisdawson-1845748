<?php
/**
 * @file
 * Helper functions for MoPublication.module
 */

/**
 *  Sort and filter the array of stored tab data
 */
function mopublication_sort_stored_tabs($tabs) {
  $tab_display = array();
  if (!empty($tabs)) {
    foreach ($tabs as $tab_id => $tab) {
      if ( empty($tab["tab_{$tab_id}_checked"]) ) continue;
      $key = (int) $tab["tab_{$tab_id}_weight"];
      $tab_display[$key] = $tab;
      $tab_display[$key]['name'] = $tab_id;
    }
    ksort($tab_display);
  }
  return $tab_display;
}

/**
 *  Return simple array of tab names
 */
function mopublication_get_tabs() {
  $tabs = mopublication_sort_stored_tabs(variable_get('mopub_tabs'));
  $ret = array();
  foreach ($tabs as $tab) {
    $ret[] = $tab['name'];
  }
  return $ret;
}

/**
 *  Return array of all category names in vocab
 *
 *  @param $vocab_id_var_name - Name of variable which contains the vocabulary ID
 *
 *  @return Array of terms with key = term ID and value = term name.
 *  Returns an empty array if vocabulary is not found.
 */
function mopublication_get_terms($vocab_id_var_name) {
  $terms = array();
  $vocab_id = variable_get($vocab_id_var_name);
  if ($vocab_id) {
    $tree = taxonomy_get_tree($vocab_id);
    foreach ($tree as $term) {
      $terms[$term->tid] = $term->name;
    }
  }
  return $terms;
}

/**
 * Get the URL for an uploaded managed file
 *
 * @param $var_name The variable that the file location is stored in
 * @param string $default  Default URL to return if file was not found
 *
 * @return bool|string
 */
function mopublication_get_file_url($var_name, $default = FALSE) {
  $path = $GLOBALS['base_path'] . drupal_get_path('module', 'mopublication');
  if (!empty($default)) $default = $path . $default;
  $ret = $default;
  if ($value = variable_get($var_name)) {
    if ($file = file_load($value)) {
      $ret = file_create_url($file->uri);
    }
  }
  return $ret;
}

/**
 * Dynamically change the logo in the demo when uploaded
 *
 * @param $file
 *
 * @return array
 */
function mopublication_process_logo($file) {
  if (!empty($file)) {
    $path = $file->destination;
    echo '<script>';
    echo "parent.jQuery('.demo_topbar img').attr('src', '" . file_create_url($path) . "');";
    echo '</script>';
  }
  return array();
}

/**
 * Dynamically change the logo to default when removed
 *
 * @param $file
 *
 * @return array
 */
function mopublication_unprocess_logo($element, $input, $form_state) {
  if (empty($input)) {
    $path = drupal_get_path('module', 'mopublication') . '/images/demo/sample_logo.png';
    echo '<script>';
    echo "parent.jQuery('.demo_topbar img').attr('src', '" . file_create_url($path) . "');";
    echo '</script>';
  }
  return array();
}

/**
 *  E-mail the completed config file to MoPublication
 */
function mopublication_email_config_file() {

  // Data needed to generate config file
  $tabs = mopublication_get_tabs();
  $categories = mopublication_get_terms('mopub_category_vocabulary');
  $tags = mopublication_get_terms('mopub_tag_vocabulary');
  $module_info = system_get_info('module', 'mopublication');

  ob_start();
  include drupal_get_path('module', 'mopublication') . '/xml/config_file.php';
  $xml = ob_get_clean();

  $success = drupal_mail('mopublication', 'config_file', 'setup@mopublication.com, chris@grenadeco.com',
    language_default(), array('xml' => htmlentities($xml)));

  return $success;
}

/**
 * Mail handler for MoPublication.module
 *
 * @param $key
 * @param $message
 * @param $params
 */
function mopublication_mail($key, &$message, $params) {
  switch ($key) {
    case 'config_file':
      $message['subject'] = t('[MoPublication] New Drupal config file submitted');
      $message['body'][] = $params['xml'];
      break;
  }
}
