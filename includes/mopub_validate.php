<?php
/**
 * @file
 * Validation functions for MoPublication
 */

/**
 * Do all the image validation
 *
 * Note: this is no longer used
 * We have switched to using managed files with built in validation
 *
 * @param $form
 * @param $form_state
 */
function mopublication_validate_images(&$form, &$form_state) {

  $image_val = array(
    'file_validate_is_image' => array(),
  );

  mopublication_do_upload('mopub_topbar_logo', $image_val, $form_state);

  /*  //add this back to enforce icon size
  $icon_val = array(
    'file_validate_is_image' => array(),
    //'file_validate_image_resolution' => array('1024x1024', '1024x1024'),
  );
  */

  mopublication_do_upload('mopub_app_icon', $image_val, $form_state);

  mopublication_do_upload('mopub_ad_iphone_splash_banner', $image_val, $form_state);
  mopublication_do_upload('mopub_ad_iphone_top_banner', $image_val, $form_state);
  mopublication_do_upload('mopub_ad_iphone_bottom_banner', $image_val, $form_state);

  mopublication_do_upload('mopub_ad_ipad_splash_banner', $image_val, $form_state);
  mopublication_do_upload('mopub_ad_ipad_top_banner', $image_val, $form_state);
  mopublication_do_upload('mopub_ad_ipad_bottom_banner', $image_val, $form_state);

}

/**
 * Upload image from form and do validation
 */
function mopublication_do_upload($field_name, $validators, &$form_state) {
  if ($tmp_file = file_save_upload($field_name, $validators)) {

    $dir = 'public://mopublication';
    file_prepare_directory($dir, FILE_CREATE_DIRECTORY);

    $destination = 'public://mopublication/' . $field_name . '_' . $tmp_file->filename;

    if ( $file = file_copy($tmp_file, $destination, FILE_EXISTS_RENAME) ) {
      $file->status = FILE_STATUS_PERMANENT;
      file_save($file);
      $form_state['values'][$field_name] = file_create_url($file->uri);
    }
    else {
      form_set_error($field_name, t("Failed to upload the image. Please check your file system permissions."));
    }
  }
  else {
    // form_set_error done by Drupal automatically
  }
}
