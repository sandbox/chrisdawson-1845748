<?php
/**
 * @file
 * MoPublication settings FormAPI form - Tab 1
 * General settings
 */

function mopublication_settings_form_1($form) {

  #====================================================================
  #  CUSTOMIZE
  #====================================================================

  $form['customize'] = array(
    '#type' => 'fieldset',
    '#title' => t('1. Customize'),
    '#group' => 'vertical_tabs',
  );

  $form['customize']['general_markup'] = array(
    '#markup' => '<h2>General</h2>',
  );

  $form['customize']['mopub_app_name'] = array(
    '#field_suffix' => '<span id="appNameStatus"></span>',
    '#type' => 'textfield',
    '#title' => t('App Name *'),
    '#size' => 40,
    '#maxlength' => 50,
    '#default_value' => variable_get('mopub_app_name'),
    '#description' => t('The official name of your app'),
  );

  $form['customize']['mopub_app_icon_name'] = array(
    '#type' => 'textfield',
    '#title' => t('App Icon Name *'),
    '#size' => 20,
    '#maxlength' => 12,
    '#default_value' => variable_get('mopub_app_icon_name'),
    '#description' => t('Displays below your app on the home screen in iOS'),
  );

  $form['customize']['mopub_app_description'] = array(
    '#type' => 'textarea',
    '#title' => t('App Description *'),
    '#default_value' => variable_get('mopub_app_description'),
    '#description' => t('A detailed description of your app as it will appear in the App Store'),
  );

  $form['customize']['mopub_app_keywords'] = array(
    '#type' => 'textarea',
    '#title' => t('App Keywords *'),
    '#rows' => 2,
    '#maxlength' => 100,
    '#default_value' => variable_get('mopub_app_keywords'),
    '#description' => t('Separate multiple keywords with commas'),
  );

  #====================================================================

  $form['customize']['topbar_markup'] = array(
    '#markup' => '<h2>Top bar</h2>',
  );

  $form['customize']['mopub_topbar_color'] = array(
    '#type' => 'textfield',
    '#title' => t('Top Bar Background Color'),
    '#size' => 10,
    '#default_value' => variable_get('mopub_topbar_color', '#5585a0'),
  );

  #====================================================================

  $form['customize']['menu_markup'] = array(
    '#markup' => '<h2>Menu</h2>',
  );

  $form['customize']['mopub_menu_bg_color'] = array(
    '#type' => 'textfield',
    '#title' => t('Menu Bar Background Color'),
    '#size' => 10,
    '#default_value' => variable_get('mopub_menu_bg_color', '#333333'),
  );

  $form['customize']['mopub_menu_active_text_color'] = array(
    '#type' => 'textfield',
    '#title' => t('Menu Bar Active Text Color'),
    '#size' => 10,
    '#default_value' => variable_get('mopub_menu_active_text_color', '#ffffff'),
  );

  $form['customize']['mopub_menu_active_bg_color'] = array(
    '#type' => 'textfield',
    '#title' => t('Menu Bar Active Background Color'),
    '#size' => 10,
    '#default_value' => variable_get('mopub_menu_active_bg_color', '#5584a0'),
  );

  $form['customize']['mopub_menu_text_color'] = array(
    '#type' => 'textfield',
    '#title' => t('Menu Bar Inactive Text Color'),
    '#size' => 10,
    '#default_value' => variable_get('mopub_menu_text_color', '#9c9c9c'),
  );

  #====================================================================
  #  Layout
  #====================================================================

  $form['customize']['layout_markup'] = array(
    '#markup' => '<h2>Layout</h2>',
  );

  $form['customize']['mopub_layout_option'] = array(
    '#theme' => 'mopublication_layout_radios',
    '#type' => 'radios',
    '#title' => t('Content Layout'),
    '#description' => t('Will determine how content is displayed on listing views'),
    '#options' => array('plain' => 'Default',
      'featured' => 'Featured',
      'page' => 'Pages'),
    '#default_value' => variable_get('mopub_layout_option', 'plain'),
  );


  #====================================================================
  #  IMAGE UPLOAD
  #====================================================================

  $form['customize']['images_markup'] = array(
    '#markup' => '<h2 style="clear: both;">Images</h2>',
  );

  //$form['#validate'][] = 'mopub_validate_images';

  $form['customize']['mopub_topbar_logo'] = array(
    '#type' => 'managed_file',
    '#title' => t('Logo Image'),
    '#description' => t('<p>Displayed at the top of your app. Use a transparent PNG for best results.</p>' .
        '<p>Recommended size: 620 x 88 pixels. Allowed formats: gif, png, jpg.</p>'
    ),
    //'#suffix' => '<div class="suffix description">Recommended size: 620 x 88 pixels. Allowed formats: gif, png, jpg.</div>',
    //not showing with managed file!  see Drupal core bug #1865172
    '#default_value' => variable_get('mopub_topbar_logo'),
    '#upload_location' => 'public://mopublication/',
    '#upload_validators' => array('file_validate_is_image' => array(),
                                'mopublication_process_logo' => array()),
    '#file_value_callbacks' => array('mopublication_unprocess_logo'),
  );

  /*
   * Don't show previews, until we can show / hide them using JS.
   *
  if ($logo_url = mopublication_get_file_url('mopub_topbar_logo')) {
    $form['customize']['mopub_topbar_logo']['#field_prefix'] =
      '<div class="settings-form-right"><div class="mopub-logo-preview"><img src="' . $logo_url . '" height="88" /></div>';
    $form['customize']['mopub_topbar_logo']['#field_suffix'] = '</div>';
  }
  */

  $form['customize']['mopub_app_icon'] = array(
    '#type' => 'managed_file',
    '#title' => t('App Icon'),
    '#description' => t('<p>This is the icon that will be used for your app on the home screen in iOS and the ' .
      'App Store. ' .
      'Please note Apple automatically rounds the corners.</p><p>Recommended size: 1024 x 1024 pixels. Allowed ' .
      'formats: gif, png, jpg.<br />Image must be square.'),
    //'#suffix' => '<div class="suffix description">Recommended size: 1024 x 1024 pixels. Allowed formats: gif, png, jpg.<br />Image must be square.</div>',   //not showing with managed file!
    '#default_value' => variable_get('mopub_app_icon'),
    '#upload_location' => 'public://mopublication/',
    '#upload_validators' => array('file_validate_is_image' => array()),
  );

  // We may want to generate a thumbnail when they upload the logo, so preview loads quicker @todo

  /*
  if ($icon_url = mopublication_get_file_url('mopub_app_icon')) {
    $form['customize']['mopub_app_icon']['#field_prefix'] =
      '<div class="settings-form-right"><div class="mopub-icon-preview"><img src="' . $icon_url . '" height="60" /></div>';
    $form['customize']['mopub_app_icon']['#field_suffix'] = '</div>';
  }
  */

  #====================================================================

  $form['customize']['content_markup'] = array(
    '#markup' => '<h2>Content Area</h2>',
  );

  $form['customize']['mopub_content_border_color'] = array(
    '#type' => 'textfield',
    '#title' => t('Content Area Border Color'),
    '#size' => 10,
    '#default_value' => variable_get('mopub_content_border_color', '#c9c9c9'),
  );

  $form['customize']['mopub_content_bg_color'] = array(
    '#type' => 'textfield',
    '#title' => t('Content Area Background Color'),
    '#size' => 10,
    '#default_value' => variable_get('mopub_content_bg_color', '#ffffff'),
  );

  #====================================================================

  $form['customize']['type_markup'] = array(
    '#markup' => '<h2>Typography</h2>',
  );

  $form['customize']['mopub_type_title_color'] = array(
    '#type' => 'textfield',
    '#title' => t('Title Color'),
    '#size' => 10,
    '#default_value' => variable_get('mopub_type_title_color', '#365b70'),
  );

  $form['customize']['mopub_type_meta_color'] = array(
    '#type' => 'textfield',
    '#title' => t('Date/Author Color'),
    '#size' => 10,
    '#default_value' => variable_get('mopub_type_meta_color', '#a3a3a3'),
  );

  $form['customize']['mopub_type_link_color'] = array(
    '#type' => 'textfield',
    '#title' => t('Link Color'),
    '#size' => 10,
    '#default_value' => variable_get('mopub_type_link_color', '#365b70'),
  );

  $form['customize']['mopub_type_body_color'] = array(
    '#type' => 'textfield',
    '#title' => t('Body Text Color'),
    '#size' => 10,
    '#default_value' => variable_get('mopub_type_body_color', '#707070'),
  );

  $form['customize']['mopub_type_font'] = array(
    '#type' => 'radios',
    '#title' => t('Font'),
    '#options' => array(
      'helvetica'       => 'Helvetica',
      'arial'           => 'Arial',
      'georgia'         => 'Georgia',
      'marion'          => 'Marion',
      'trebuchet ms'    => 'Trebuchet MS',
      'times new roman' => 'Times New Roman'
    ),
    '#description' => t('This font will be used for all dynamic content in the app'),
    '#default_value' => variable_get('mopub_type_font', 'helvetica'),
  );

  return $form;

}
