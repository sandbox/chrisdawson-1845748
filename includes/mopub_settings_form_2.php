<?php
/**
 * @file
 * MoPublication settings FormAPI form - Tab 2
 * Content Management
 */

function mopublication_settings_form_2($form) {

  $node_types = node_type_get_types();

  $video_node_type_options = array('-' => '-');
  $audio_node_type_options = array('-' => '-');
  $article_node_type_options = array();

  foreach ($node_types as $node_type) {
    $article_node_type_options[$node_type->type] = $node_type->name;
    $audio_node_type_options[$node_type->type] = $node_type->name;
    $video_node_type_options[$node_type->type] = $node_type->name;
  }

  #====================================================================
  #  CONTENT MANAGEMENT
  #====================================================================

  $form['content'] = array(
    '#type' => 'fieldset',
    '#title' => t('2. Content Management'),
    '#group' => 'vertical_tabs',
  );

  $form['content']['categories_markup'] = array(
    '#markup' => '<h2>Content</h2>',
  );

  $vocabs_options = array();
  $vocabs = taxonomy_get_vocabularies();

  foreach ($vocabs as $vid => $vocab) {
    $vocabs_options[$vid] = $vocab->name;
  }

  $form['content']['mopub_article_node_type'] = array(
    '#type' => 'select',
    '#title' => t('Node Type'),
    '#description' => t("Select the node type you'd like to use for content in your app"),
    '#options' => $article_node_type_options,
    '#default_value' => variable_get('mopub_article_node_type', 'article'),
  );

  $form['content']['mopub_category_vocabulary'] = array(
    '#type' => 'select',
    '#title' => t('Category Vocabulary'),
    '#description' => t("Select the Taxonomy Vocabulary you" .
      "'d like to use for Categories. The terms from the Vocabulary will be displayed as a menu. ") .
      l(t('Set up Taxonomies'), 'admin/structure/taxonomy') . '.',
    '#options' => $vocabs_options,
    '#default_value' => variable_get('mopub_category_vocabulary'),
  );

  $form['content']['mopub_tag_vocabulary'] = array(
    '#type' => 'select',
    '#title' => t('Tag Vocabulary'),
    '#description' => t("Select the Taxonomy Vocabulary you" .
      "'d like to use for tags. ") .
      l(t('Set up Taxonomies'), 'admin/structure/taxonomy') . '.',
    '#options' => $vocabs_options,
    '#default_value' => variable_get('mopub_tag_vocabulary'),
  );

  $image_field_options = array('-' => '-');
  $audio_field_options = array('-' => '-');
  $video_field_options = array('-' => '-');

  $fields = field_info_fields();

  foreach ($fields as $field) {
    if ( $field['type'] == 'image' ) {
      $image_field_options[$field['field_name']] = $field['field_name'];
    }
    elseif ( $field['type'] == 'file' ) {
      $audio_field_options[$field['field_name']] = $field['field_name'];
      $video_field_options[$field['field_name']] = $field['field_name'];
    }
  }

  if (count($image_field_options) == 1) {
    $image_field_options = array('disabled' => 'No Image fields found.');
  }

  $form['content']['mopub_image_field'] = array(
    '#type' => 'select',
    '#title' => t('Image Field'),
    '#description' => t("Select the Image field you" .
      "'d like to use to display images in your app content"),
    '#options' => $image_field_options,
    '#default_value' => variable_get('mopub_image_field', 'field_image'),
  );

  #====================================================================
  #  TAB SELECTION
  #====================================================================

  $form['content']['tabs_markup'] = array(
    '#markup' => '<h2>Tab menu</h2>',
  );

  $tab_vars = variable_get('mopub_tabs');

  $form['content']['mopub_tabs'] = array();

  $available_tabs = array(
    array(
      'name' => 'Latest',
      'weight' => isset($tab_vars['latest']['tab_latest_weight']) ? $tab_vars['latest']['tab_latest_weight'] : 0,
      'required' => TRUE,
    ),

    array(
      'name' => 'About',
      'weight' => isset($tab_vars['about']['tab_about_weight']) ? $tab_vars['about']['tab_about_weight'] : 0,
      'required' => TRUE,
    ),

    array(
      'name' => 'Contact',
      'weight' => isset($tab_vars['contact']['tab_contact_weight']) ? $tab_vars['contact']['tab_contact_weight'] : 0,
    ),

    array(
      'name' => 'Categories',
      'weight' => isset($tab_vars['categories']['tab_categories_weight']) ? $tab_vars['categories']['tab_categories_weight'] : 0,
    ),

    array(
      'name' => 'Tags',
      'weight' => $tab_vars['tags']['tab_tags_weight'],
    ),

    array(
      'name' => 'Search',
      'weight' => $tab_vars['search']['tab_search_weight'],
    ),

    array(
      'name' => 'Audio',
      'weight' => $tab_vars['audio']['tab_audio_weight'],
    ),

    array(
      'name' => 'Video',
      'weight' => $tab_vars['video']['tab_video_weight'],
    ),

  );

  $form['content']['mopub_tabs']['#tree'] = TRUE;
  $form['content']['mopub_tabs']['#theme'] = 'mopublication_settings_tabs';

  usort($available_tabs, 'mopublication_sort_tabs');

  foreach ($available_tabs as $tab) {

    $tab_id = drupal_strtolower($tab['name']);

    if ( ! empty($tab['required']) ) {
      $tab['checked'] = TRUE;
    }
    else {
      $tab['checked'] = ( isset($tab_vars[$tab_id]['tab_' . $tab_id . '_checked']) &&
        $tab_vars[$tab_id]['tab_' . $tab_id . '_checked'] == TRUE ) ? TRUE : FALSE;
    }

    $form['content']['mopub_tabs'][$tab_id] = array(

      'tab_' . $tab_id . '_checked' => array(
        '#type' => 'checkbox',
        '#title' => $tab['name'],
        '#default_value' => isset($tab['checked']) ? $tab['checked'] : 0,
        '#disabled' => !empty($tab['required']),  //if this is a required tab, don't let user de-select it
      ),

      'tab_' . $tab_id . '_weight' => array(
        '#type' => 'weight',
        '#delta' => count($available_tabs),
        '#default_value' => isset($tab['weight']) ? $tab['weight'] : 0,
      ),

    );
  }


  #====================================================================

  $form['content']['about_markup'] = array(
    '#markup' => '<h2>About Screen</h2>',
  );

  $form['content']['mopub_content_about'] = array(
    '#type' => 'textarea',
    '#title' => t('About Screen'),
    '#description' => t('A brief description of your company which will appear on the "About" screen'),
    '#rows' => 4,
    '#default_value' => variable_get('mopub_content_about'),
  );

  $form['content']['about_markup'] = array(
    '#markup' => '<h2>Contact Screen</h2>',
  );

  $form['content']['mopub_contact_email'] = array(
    '#type' => 'textfield',
    '#title' => t('Email address'),
    '#description' => t('Enter an email address if you want your app users to be able to contact you. This will' .
      ' appear on the "Contact" screen if selected.'),
    '#size' => 40,
    '#default_value' => variable_get('mopub_contact_email'),
  );

  $form['content']['mopub_content_contact'] = array(
    '#type' => 'textarea',
    '#title' => t('General Contact Information'),
    '#description' => t('General information such as business address or telephone number. This will appear' .
      ' on the "Contact" screen if selected.'),
    '#rows' => 4,
    '#default_value' => variable_get('mopub_content_contact'),
  );

  #--------------------------------------------------------------------
  #  AUDIO AND VIDEO
  #--------------------------------------------------------------------

  $form['content']['video_audio_markup'] = array(
    '#markup' => '<h2>Audio & Video</h2>',
  );

  $form['content']['mopub_audio_node_type'] = array(
    '#type' => 'select',
    '#title' => t('Audio Node Type'),
    '#description' => t("Select the node type you" .
      "'d like to use for audio"),
    '#options' => $audio_node_type_options,
    '#default_value' => variable_get('mopub_audio_node_type'),
  );

  $form['content']['mopub_audio_field'] = array(
    '#type' => 'select',
    '#title' => t('Audio Field'),
    '#description' => t("Select the field that contains your audio files"),
    '#options' => $audio_field_options,
    '#default_value' => variable_get('mopub_audio_field'),
  );

  $form['content']['mopub_video_node_type'] = array(
    '#type' => 'select',
    '#title' => t('Video Node Type'),
    '#description' => t("Select the node type you'd like to use for videos"),
    '#options' => $video_node_type_options,
    '#default_value' => variable_get('mopub_video_node_type'),
  );

  $form['content']['mopub_video_field'] = array(
    '#type' => 'select',
    '#title' => t('Video Field'),
    '#description' => t("Select the field that contains your video files"),
    '#options' => $video_field_options,
    '#default_value' => variable_get('mopub_video_field'),
  );


  #====================================================================

  $form['content']['comments_markup'] = array(
    '#markup' => '<h2>Comments</h2>',
  );

  $form['content']['mopub_comment_option'] = array(
    '#type' => 'select',
    '#title' => t('In-app comment option'),
    '#description' => t("Enable commenting in the app using the Drupal Comment module or Disqus comments."),
    '#options' => array(
      'none' => 'None',
      'drupal' => 'Drupal (anonymous user)',
      'disqus' => 'Disqus',
    ),
    '#default_value' => variable_get('mopub_comment_option'),
  );

  $form['content']['mopub_anonymous_comments_allowed'] = array(
    '#type'  => 'hidden',
    '#value' => $comments_available = user_access('post comments', drupal_anonymous_user()),
  );

  $form['content']['disqus'] = array(
    '#type' => 'container',
    '#states' => array('visible' => array(':input[name="mopub_comment_option"]' => array('value' => 'disqus'))),
  );

  // Get Disqus short code from the disqus module, if it's available
  $disqus_default = variable_get('mopub_disqus_short_name');
  if ( empty($disqus_default) && variable_get('disqus_domain', FALSE) ) {
    $disqus_default = variable_get('disqus_domain');
  }

  $form['content']['disqus']['mopub_disqus_short_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Disqus Short Name'),
    '#size' => 30,
    '#default_value' => $disqus_default,
  );

  /*
   * Removed until we would like to integrate with Disqus API
   *
  $form['content']['disqus']['mopub_disqus_api_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Disqus API Key'),
    '#size' => 30,
    '#default_value' => variable_get('mopub_disqus_api_key'),
  );

  $form['content']['disqus']['mopub_disqus_user_api_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Disqus User API Key'),
    '#size' => 30,
    '#default_value' => variable_get('mopub_disqus_user_api_key'),
  );
  */

  return $form;

}

/**
 *  Sort the tabs for display in the settings form
 */
function mopublication_sort_tabs($a, $b) {
  if ($a['weight'] == $b['weight']) {
    return 0;
  }
  return ($a['weight'] < $b['weight']) ? -1 : 1;
}

