<?php
/**
 *  @file
 *  Get and prepare the data for display in the MoPublication demo
 */

$query = db_select('node', 'n')
  ->fields('n', array('nid', 'title', 'created'))
  ->condition('status', 1)  // published
  ->condition('type', variable_get('mopub_article_node_type', 'article'))  //article node type
  ->orderBy('created', 'DESC') // most recent first
  ->range(0, 10)
  ->execute();

$data = $query->fetchAllAssoc('nid');
$nodes = node_load_multiple(array_keys($data));

$categories = taxonomy_get_tree(variable_get('mopub_content_vocabulary'));

$logo_url = mopublication_get_file_url('mopub_topbar_logo', '/images/demo/sample_logo.png');
$ad_url = mopublication_get_file_url('mopub_ad_iphone_top_banner', '/images/demo/sample_ad_banner.png');

$tabs = mopublication_get_tabs();

$image_field = variable_get('mopub_image_field', 'field_image');

$iphone_ad_display = (variable_get('mopub_ad_option') == 'none') ? 'none' : 'block';
