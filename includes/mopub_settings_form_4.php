<?php
/**
 * @file
 * MoPublication settings FormAPI form - Tab 4
 * Advanced settings
 */

require_once "app_store_options.php";

/**
 * MoPublication settings FormAPI form - Tab 4
 * Advanced
 */
function mopublication_settings_form_4($form) {

  #====================================================================
  #  ADVANCED
  #====================================================================

  $form['advanced'] = array(
    '#type' => 'fieldset',
    '#title' => t('4. Advanced'),
    '#group' => 'vertical_tabs',
  );

  //--------------------------------------------------------------
  //  Language

  $form['advanced']['language_markup'] = array(
    '#markup' => '<h2>Language</h2>',
  );

  $form['advanced']['mopub_language'] = array(
    '#type' => 'select',
    '#title' => t('Content Language'),
    '#options' => mopublication_get_languages(),
    '#default_value' => variable_get('mopub_language', 'English'),
  );

  //--------------------------------------------------------------
  //  Countries

  $form['advanced']['countries_markup'] = array(
    '#markup' => '<h2>Countries</h2>',
  );

  $form['advanced']['mopub_countries_option'] = array(
    '#type' => 'select',
    '#title' => t('Publish to these App Stores'),
    '#options' => array(
      'all' => 'All',
      'choose' => 'Let me choose',
    ),
    '#default_value' => variable_get('mopub_countries_option'),
  );

  $form['advanced']['mopub_stores'] = array(
    '#type' => 'container',
    '#states' => array('visible' => array(':input[name="mopub_countries_option"]' => array('value' => 'choose'))),
  );

  $form['advanced']['mopub_stores']['mopub_countries'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Countries'),
    '#options' => mopublication_get_countries(),
    '#default_value' => variable_get('mopub_countries', mopublication_get_countries()),
    '#field_prefix' => '<div class="settings-form-right"><div class="select_all"><input type="checkbox" id="select_all_countries"> Select all</div>',
    '#field_suffix' => '</div><div style="clear: left;"></div>',
  );

  //--------------------------------------------------------------
  //  Categories

  $form['advanced']['categories_markup'] = array(
    '#markup' => '<h2>Categories</h2>',
  );

  $form['advanced']['mopub_category_primary'] = array(
    '#type' => 'select',
    '#title' => t('Primary Category'),
    '#options' => mopublication_get_categories(),
    '#default_value' => variable_get('mopub_category_primary', 'News'),
  );

  $form['advanced']['mopub_category_secondary'] = array(
    '#type' => 'select',
    '#title' => t('Secondary Category'),
    '#options' => array_merge(array('-' => '-'), mopublication_get_categories()),
    '#default_value' => variable_get('mopub_category_secondary', 'None'),
  );

  //--------------------------------------------------------------
  //  Age Restriction

  $form['advanced']['age_markup'] = array(
    '#markup' => '<h2>Age Restriction</h2>' .
                 '<p>Please rate your app content</p>',
  );

  $form['advanced']['mopub_age_fantasy_violence'] = array(
    '#type' => 'select',
    '#title' => t('Cartoon or Fantasy Violence'),
    '#options' => array('none' => 'None', 'mild' => 'Infrequent/Mild', 'intense' => 'Frequent/Intense'),
    '#default_value' => variable_get('mopub_age_fantasy_violence', 'None'),
  );

  $form['advanced']['mopub_age_realistic_violence'] = array(
    '#type' => 'select',
    '#title' => t('Realistic Violence'),
    '#options' => array('none' => 'None', 'mild' => 'Infrequent/Mild', 'intense' => 'Frequent/Intense'),
    '#default_value' => variable_get('mopub_age_realistic_violence', 'None'),
  );

  $form['advanced']['mopub_age_sexual'] = array(
    '#type' => 'select',
    '#title' => t('Sexual Content or Nudity'),
    '#options' => array('none' => 'None', 'mild' => 'Infrequent/Mild', 'intense' => 'Frequent/Intense'),
    '#default_value' => variable_get('mopub_age_sexual', 'None'),
  );

  $form['advanced']['mopub_age_profanity'] = array(
    '#type' => 'select',
    '#title' => t('Profanity or Crude Humor'),
    '#options' => array('none' => 'None', 'mild' => 'Infrequent/Mild', 'intense' => 'Frequent/Intense'),
    '#default_value' => variable_get('mopub_age_profanity', 'None'),
  );

  $form['advanced']['mopub_age_drug'] = array(
    '#type' => 'select',
    '#title' => t('Alcohol, Tobacco, or Drug Use or References'),
    '#options' => array('none' => 'None', 'mild' => 'Infrequent/Mild', 'intense' => 'Frequent/Intense'),
    '#default_value' => variable_get('mopub_age_drug', 'None'),
  );

  $form['advanced']['mopub_age_mature'] = array(
    '#type' => 'select',
    '#title' => t('Mature/Suggestive Themes'),
    '#options' => array('none' => 'None', 'mild' => 'Infrequent/Mild', 'intense' => 'Frequent/Intense'),
    '#default_value' => variable_get('mopub_age_mature', 'None'),
  );

  $form['advanced']['mopub_age_gambling'] = array(
    '#type' => 'select',
    '#title' => t('Simulated Gambling'),
    '#options' => array('none' => 'None', 'mild' => 'Infrequent/Mild', 'intense' => 'Frequent/Intense'),
    '#default_value' => variable_get('mopub_age_gambling', 'None'),
  );

  $form['advanced']['mopub_age_horror'] = array(
    '#type' => 'select',
    '#title' => t('Horror/Fear Themes'),
    '#options' => array('none' => 'None', 'mild' => 'Infrequent/Mild', 'intense' => 'Frequent/Intense'),
    '#default_value' => variable_get('mopub_age_horror', 'None'),
  );

  $form['advanced']['mopub_age_graphic_violence'] = array(
    '#type' => 'select',
    '#title' => t('Prolonged Graphic or Sadistic Realistic Violence'),
    '#options' => array('none' => 'None', 'mild' => 'Infrequent/Mild', 'intense' => 'Frequent/Intense'),
    '#default_value' => variable_get('mopub_age_graphic_violence', 'None'),
  );

  $form['advanced']['mopub_age_graphic_sexual'] = array(
    '#type' => 'select',
    '#title' => t('Graphic Sexual Content and Nudity'),
    '#options' => array('none' => 'None', 'mild' => 'Infrequent/Mild', 'intense' => 'Frequent/Intense'),
    '#default_value' => variable_get('mopub_age_graphic_sexual', 'None'),
  );

  return $form;
}

