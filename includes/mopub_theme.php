<?php
/**
 * @file
 * Theme hook implementations for MoPublication.module
 */

/**
 * Implement hook_theme().
 */
function mopublication_theme() {
  return array(
    'mopublication_settings_tabs' => array(
      'render element' => 'form',
    ),
    'mopublication_layout_radios' => array(
      'render element' => 'form',
    ),
  );
}

/**
 *  Render MoPublication tab selection table
 */
function theme_mopublication_settings_tabs($variables) {

  $tabs = $variables['form'];

  $output = '';

  drupal_add_tabledrag('mopub_tab_selection', 'order', 'sibling', 'weight-group', NULL, NULL, TRUE);

  $rows = array();
  $header = array('Tab', 'Weight');

  foreach (element_children($tabs) as $key) {
    $element = &$tabs[$key];
    $element['tab_' . $key . '_weight']['#attributes']['class'] = array('weight-group');

    $row = array();
    $row[] = drupal_render($element['tab_' . $key . '_checked']);
    $row[] = drupal_render($element['tab_' . $key . '_weight']) . drupal_render($element['id']);
    $rows[] = array('data' => $row, 'class' => array('draggable'));
  }

  $output .= theme('table', array(
    'header' => $header,
    'rows' => $rows,
    'attributes' => array('id' => 'mopub_tab_selection'),
  ));

  return $output;
}

/**
 *  Render the Layout option radios
 */
function theme_mopublication_layout_radios($variables) {

  $radio = $variables['form'];

  $output = '<table class="mopub-layout-options" style="width: 430px;"><tr>';
  foreach ($radio['#options'] as $key => $value ) {
    $output .= '<td>';
    $output .= '<label for="' . $radio['#id'] . '-' . $key . '" class="mopub-layout-icon-' . $key . '">&nbsp;</label>';
    $output .= '<div><input type="radio" id="' . $radio['#id'] . '-' . $key . '" ';
    if ($radio['#default_value'] == $key) {
      $output .= 'checked="checked" ';
    }
    $output .= 'name="' . $radio['#name'] . '" value="' . $key . '"> ';
    $output .= '<label style="display: inline;" for="' . $radio['#id'] . '-' . $key . '">' . $value . '</label></div>';
    $output .= '</td>';
  }
  $output .= '</tr></table>';

  return $output;
}

