<?php
/**
 * @file
 * MoPublication settings FormAPI form - Tab 3
 * Third-party integration options
 */

function mopublication_settings_form_3($form) {

  #====================================================================
  #  THIRD-PARTY INTEGRATION
  #====================================================================

  $form['third_party'] = array(
    '#type' => 'fieldset',
    '#title' => t('3. Third-Party Integration'),
    '#group' => 'vertical_tabs',
  );

  $form['third_party']['statistics_markup'] = array(
    '#markup' => '<h2>Statistics</h2>',
  );

  $form['third_party']['mopub_analytics_active'] = array(
    '#type' => 'select',
    '#title' => t('Use Google Analytics?'),
    '#options' => array('no' => 'No', 'yes' => 'Yes'),
    '#default_value' => variable_get('mopub_analytics_active', 'no'),
  );

  $form['third_party']['mopub_analytics_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Web Property ID (UA Number)'),
    '#description' => t('This is shown in your Google Analytics account. '
      . '<a href="http://support.google.com/analytics/bin/answer.py?hl=en&answer=1032385"'
      . 'title="Google Analytics Web Property ID" target="blank">Click here</a> if you need help finding it.'),
    '#default_value' => variable_get('mopub_analytics_id'),
    '#size' => 25,
    '#states' => array('visible' => array(':input[name="mopub_analytics_active"]' => array('value' => 'yes'))),
  );

  #====================================================================

  $form['third_party']['sharing_markup'] = array(
    '#markup' => '<h2>Social Sharing</h2>',
  );

  $form['third_party']['mopub_sharing_option'] = array(
    '#type' => 'select',
    '#title' => t('Sharing Option'),
    '#options' => array(
      'default'    => 'Default',
      'my_own'     => 'Use my own AddThis account',
      'no_sharing' => 'No sharing'
    ),
    '#default_value' => variable_get('mopub_sharing_option', 'default'),
  );

  // Use a container to show / hide all Sharing options at once

  $form['third_party']['sharing'] = array(
    '#type' => 'container',
    '#states' => array('visible' => array(':input[name="mopub_sharing_option"]' => array('value' => 'my_own'))),
  );

  $form['third_party']['sharing']['mopub_addthis_app_id'] = array(
    '#type' => 'textfield',
    '#title' => t('AddThis Application ID'),
    '#description' => t('The Application ID which you can find inside the profile section ' .
                        'of your AddThis account'),
    '#default_value' => variable_get('mopub_addthis_app_id'),
    '#size' => 25,
    '#group' => 'mopub_sharing_container',
  );

  $form['third_party']['sharing']['mopub_addthis_publisher_id'] = array(
    '#type' => 'textfield',
    '#title' => t('AddThis Publisher ID'),
    '#description' => t('This is your AddThis Publisher ID'),
    '#default_value' => variable_get('mopub_addthis_publisher_id'),
    '#size' => 25,
  );

  $form['third_party']['sharing']['mopub_addthis_username'] = array(
    '#type' => 'textfield',
    '#title' => t('AddThis User Name'),
    '#description' => t('The username that you use to log into your AddThis account'),
    '#suffix' => t('<div class="suffix description">You can create a username by accessing the ' .
      '<a href="http://www.addthis.com/forum/" target="_blank">AddThis Forum</a></div>'),
    '#default_value' => variable_get('mopub_addthis_username'),
    '#size' => 25,
  );

  $form['third_party']['sharing']['mopub_addthis_twitter'] = array(
    '#type' => 'textfield',
    '#title' => t('Twitter User Name'),
    '#description' => t('Enter your Twitter username/handle if you have a Twitter account'),
    '#default_value' => variable_get('mopub_addthis_twitter'),
    '#size' => 25,
  );

  #====================================================================

  $form['third_party']['ad_markup'] = array(
    '#markup' => '<h2>Ad Management</h2>',
  );

  $form['third_party']['mopub_ad_option'] = array(
    '#type' => 'select',
    '#title' => t('Select Ad Option'),
    '#options' => array(
      'none'       => 'None',
      'custom_ads' => 'Custom Ads',
      'ad_server'  => 'Ad Server',
      'admob'      => 'AdMob'
    ),
    '#default_value' => variable_get('mopub_ad_option', 'none'),
  );

  // Use containers to show / hide all Ad options at once

  $form['third_party']['ad_custom'] = array(
    '#type' => 'container',
    '#states' => array('visible' => array(':input[name="mopub_ad_option"]' => array('value' => 'custom_ads'))),
  );

  $form['third_party']['ad_custom']['ad_custom_markup'] = array(
    '#markup' => '<h3>Custom Ad</h3>' .
            '<p>Upload your own custom banners and enter a destination URL for the clickthrough</p>',
  );

  //--------------------------------------------------------------
  //  IPHONE CUSTOM ADS
  //--------------------------------------------------------------

  $form['third_party']['ad_custom']['custom_iphone_markup'] = array(
    '#markup' => '<h4>iPhone</h4>',
  );

  $form['third_party']['ad_custom']['mopub_ad_iphone_splash_url'] = array(
    '#type' => 'textfield',
    '#title' => t('iPhone Splash Destination URL'),
    '#default_value' => variable_get('mopub_ad_iphone_splash_url'),
    '#size' => 40,
  );

  $form['third_party']['ad_custom']['mopub_ad_iphone_splash_banner'] = array(
    '#type' => 'managed_file',
    '#title' => t('iPhone Splash Banner Advert'),
    '#default_value' => variable_get('mopub_ad_iphone_splash_banner'),
    '#size' => 40,
    '#suffix' => '<div class="suffix description">Low-res: 320 x 416 pixels / Hi-res: 640 x 832 pixels.<br />' .
                 'Allowed formats: png, jpg, gif.</div>',
    '#upload_location' => 'public://mopublication/',
  );

/*  if ($isb_url = variable_get('mopub_ad_iphone_splash_banner')) {
    $form['third_party']['ad_custom']['mopub_ad_iphone_splash_banner']['#field_prefix'] =
      '<div class="settings-form-right"><div class="mopub-image-preview"><img src="' . $isb_url . '" height="100" /></div>';
    $form['third_party']['ad_custom']['mopub_ad_iphone_splash_banner']['#field_suffix'] = '</div>';
  } */

  //--------------------------------------------------------------

  $form['third_party']['ad_custom']['mopub_ad_iphone_top_url'] = array(
    '#type' => 'textfield',
    '#title' => t('iPhone Top Ad Destination URL'),
    '#default_value' => variable_get('mopub_ad_iphone_top_url'),
    '#size' => 40,
  );

  $form['third_party']['ad_custom']['mopub_ad_iphone_top_banner'] = array(
    '#type' => 'managed_file',
    '#title' => t('iPhone Top Ad Banner Advert'),
    '#default_value' => variable_get('mopub_ad_iphone_top_banner'),
    '#size' => 40,
    '#suffix' => '<div class="suffix description">Low-res: 320 x 80 pixels / Hi-res: 640 x 160 pixels.<br />' .
      'Allowed formats: png, jpg, gif.</div>',
    '#upload_location' => 'public://mopublication/',
  );

/*  if ($itb_url = variable_get('mopub_ad_iphone_top_banner')) {
    $form['third_party']['ad_custom']['mopub_ad_iphone_top_banner']['#field_prefix'] =
      '<div class="settings-form-right"><div class="mopub-image-preview"><img src="' . $itb_url . '" height="80" /></div>';
    $form['third_party']['ad_custom']['mopub_ad_iphone_top_banner']['#field_suffix'] = '</div>';
  } */

  //--------------------------------------------------------------

  $form['third_party']['ad_custom']['mopub_ad_iphone_bottom_url'] = array(
    '#type' => 'textfield',
    '#title' => t('iPhone Bottom Ad Destination URL'),
    '#default_value' => variable_get('mopub_ad_iphone_bottom_url'),
    '#size' => 40,
  );

  $form['third_party']['ad_custom']['mopub_ad_iphone_bottom_banner'] = array(
    '#type' => 'managed_file',
    '#title' => t('iPhone Bottom Ad Banner Advert'),
    '#default_value' => variable_get('mopub_ad_iphone_bottom_banner'),
    '#size' => 40,
    '#suffix' => '<div class="suffix description">Low-res: 320 x 80 pixels / Hi-res: 640 x 160 pixels.<br />' .
      'Allowed formats: png, jpg, gif.</div>',
    '#upload_location' => 'public://mopublication/',
  );

/*  if ($ibb_url = variable_get('mopub_ad_iphone_bottom_banner')) {
    $form['third_party']['ad_custom']['mopub_ad_iphone_bottom_banner']['#field_prefix'] =
      '<div class="settings-form-right"><div class="mopub-image-preview"><img src="' . $ibb_url . '" height="80" /></div>';
    $form['third_party']['ad_custom']['mopub_ad_iphone_bottom_banner']['#field_suffix'] = '</div>';
  } */

  //--------------------------------------------------------------
  //  IPAD CUSTOM ADS
  //--------------------------------------------------------------

  $form['third_party']['ad_custom']['custom_ipad_markup'] = array(
    '#markup' => '<h4>iPad</h4>',
  );

  $form['third_party']['ad_custom']['mopub_ad_ipad_splash_url'] = array(
    '#type' => 'textfield',
    '#title' => t('iPad Splash Destination URL'),
    '#default_value' => variable_get('mopub_ad_ipad_splash_url'),
    '#size' => 40,
  );

  $form['third_party']['ad_custom']['mopub_ad_ipad_splash_banner'] = array(
    '#type' => 'managed_file',
    '#title' => t('iPad Splash Banner Advert'),
    '#default_value' => variable_get('mopub_ad_ipad_splash_banner'),
    '#size' => 40,
    '#suffix' => '<div class="suffix description">Low-res: 768 x 960 pixels / Hi-res: 1536 x 1920 pixels.<br />' .
                 'Allowed formats: png, jpg, gif.</div>',
    '#upload_location' => 'public://mopublication/',
  );

/*  if ($dsb_url = variable_get('mopub_ad_ipad_splash_banner')) {
    $form['third_party']['ad_custom']['mopub_ad_ipad_splash_banner']['#field_prefix'] =
      '<div class="settings-form-right"><div class="mopub-image-preview"><img src="' . $dsb_url . '" height="100" /></div>';
    $form['third_party']['ad_custom']['mopub_ad_ipad_splash_banner']['#field_suffix'] = '</div>';
  } */

  //--------------------------------------------------------------

  $form['third_party']['ad_custom']['mopub_ad_ipad_top_url'] = array(
    '#type' => 'textfield',
    '#title' => t('iPad Top Ad Destination URL'),
    '#default_value' => variable_get('mopub_ad_ipad_top_url'),
    '#size' => 40,
  );

  $form['third_party']['ad_custom']['mopub_ad_ipad_top_banner'] = array(
    '#type' => 'managed_file',
    '#title' => t('iPad Top Ad Banner Advert'),
    '#default_value' => variable_get('mopub_ad_ipad_top_banner'),
    '#size' => 40,
    '#suffix' => '<div class="suffix description">Low-res: 728 x 90 pixels / Hi-res: 1456 x 180 pixels.<br />' .
      'Allowed formats: png, jpg, gif.</div>',
    '#upload_location' => 'public://mopublication/',
  );

/*  if ($dtb_url = variable_get('mopub_ad_ipad_top_banner')) {
    $form['third_party']['ad_custom']['mopub_ad_ipad_top_banner']['#field_prefix'] =
      '<div class="settings-form-right"><div class="mopub-image-preview"><img src="' . $dtb_url . '" height="90" /></div>';
    $form['third_party']['ad_custom']['mopub_ad_ipad_top_banner']['#field_suffix'] = '</div>';
  } */

  //--------------------------------------------------------------

  $form['third_party']['ad_custom']['mopub_ad_ipad_bottom_url'] = array(
    '#type' => 'textfield',
    '#title' => t('iPad Bottom Ad Destination URL'),
    '#default_value' => variable_get('mopub_ad_ipad_bottom_url'),
    '#size' => 40,
  );

  $form['third_party']['ad_custom']['mopub_ad_ipad_bottom_banner'] = array(
    '#type' => 'managed_file',
    '#title' => t('iPad Bottom Ad Banner Advert'),
    '#default_value' => variable_get('mopub_ad_ipad_bottom_banner'),
    '#size' => 40,
    '#suffix' => '<div class="suffix description">Low-res: 728 x 90 pixels / Hi-res: 1456 x 180 pixels.<br />' .
      'Allowed formats: png, jpg, gif.</div>',
    '#upload_location' => 'public://mopublication/',
  );

/*  if ($dbb_url = variable_get('mopub_ad_ipad_bottom_banner')) {
    $form['third_party']['ad_custom']['mopub_ad_ipad_bottom_banner']['#field_prefix'] =
      '<div class="settings-form-right"><div class="mopub-image-preview"><img src="' . $dbb_url . '" height="90" /></div>';
    $form['third_party']['ad_custom']['mopub_ad_ipad_bottom_banner']['#field_suffix'] = '</div>';
  } */

  //--------------------------------------------------------------
  //  AD SERVER
  //--------------------------------------------------------------

  $form['third_party']['ad_server'] = array(
    '#type' => 'container',
    '#states' => array('visible' => array(':input[name="mopub_ad_option"]' => array('value' => 'ad_server'))),
  );

  $form['third_party']['ad_server']['ad_server_markup'] = array(
    '#markup' => '<h3>Ad Server</h3>' .
      '<p>Paste the relevant embed code into the desired positions.' .
      '<br />Google DFP, ADTECH and OpenX are currently supported.</p>',
  );

  $form['third_party']['ad_server']['embed_iphone_markup'] = array(
    '#markup' => '<h4>iPhone</h4>',
  );

  $form['third_party']['ad_server']['mopub_ad_embed_iphone_splash'] = array(
    '#type' => 'textarea',
    '#title' => t('iPhone Splash Page'),
    '#suffix' => t('<div class="suffix description">Low-res: 320 x 416 pixels / Hi-res: 640 x 832 pixels</div>'),
    '#default_value' => variable_get('mopub_ad_embed_iphone_splash'),
  );

  $form['third_party']['ad_server']['mopub_ad_embed_iphone_top'] = array(
    '#type' => 'textarea',
    '#title' => t('iPhone Top Ad'),
    '#suffix' => t('<div class="suffix description">Low-res: 320 x 80 pixels / Hi-res: 640 x 160 pixels</div>'),
    '#default_value' => variable_get('mopub_ad_embed_iphone_top'),
  );

  $form['third_party']['ad_server']['mopub_ad_embed_iphone_bottom'] = array(
    '#type' => 'textarea',
    '#title' => t('iPhone Bottom Ad'),
    '#suffix' => t('<div class="suffix description">Low-res: 320 x 80 pixels / Hi-res: 640 x 160 pixels</div>'),
    '#default_value' => variable_get('mopub_ad_embed_iphone_bottom'),
  );

  $form['third_party']['ad_server']['embed_ipad_markup'] = array(
    '#markup' => '<h4>iPad</h4>',
  );

  $form['third_party']['ad_server']['mopub_ad_embed_ipad_splash'] = array(
    '#type' => 'textarea',
    '#title' => t('iPad Splash Page'),
    '#suffix' => t('<div class="suffix description">Low-res: 768 x 960 pixels / Hi-res: 1536 x 1920 pixels</div>'),
    '#default_value' => variable_get('mopub_ad_embed_ipad_splash'),
  );

  $form['third_party']['ad_server']['mopub_ad_embed_ipad_top'] = array(
    '#type' => 'textarea',
    '#title' => t('iPad Top Ad'),
    '#suffix' => t('<div class="suffix description">Low-res: 728 x 90 pixels / Hi-res: 1456 x 180 pixels</div>'),
    '#default_value' => variable_get('mopub_ad_embed_ipad_top'),
  );

  $form['third_party']['ad_server']['mopub_ad_embed_ipad_bottom'] = array(
    '#type' => 'textarea',
    '#title' => t('iPad Bottom Ad'),
    '#suffix' => t('<div class="suffix description">Low-res: 728 x 90 pixels / Hi-res: 1456 x 180 pixels</div>'),
    '#default_value' => variable_get('mopub_ad_embed_ipad_bottom'),
  );

  //--------------------------------------------------------------
  //  ADMOB
  //--------------------------------------------------------------

  $form['third_party']['admob'] = array(
    '#type' => 'container',
    '#states' => array('visible' => array(':input[name="mopub_ad_option"]' => array('value' => 'admob'))),
  );

  $form['third_party']['admob']['admob_markup'] = array(
    '#markup' => '<h3>AdMob</h3>' .
      '<p>Enter your AdMob publisher ID to start serving ads from the AdMob network</p>',
  );

  $form['third_party']['admob']['mopub_admob_id'] = array(
    '#type' => 'textfield',
    '#title' => t('AdMob ID'),
    '#description' => t('Your unique ID supplied by AdMob'),
    '#default_value' => variable_get('mopub_admob_id'),
    '#size' => 25,
  );

  return $form;
}
