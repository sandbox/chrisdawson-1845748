<?php
/**
 *  @file
 *  Live demo (preview) of the app
 */
require_once "mopub_demo_content.php";

?>
<div id="mopub_demo">
  <div class="phone">
    <div class="viewer <?php echo variable_get('mopub_layout_option', 'plain'); ?>">

      <div class="demo_topbar_background">
        <div class="demo_topbar">
          <img src="<?php echo $logo_url ?>" height="32" />
        </div>
      </div>

      <div class="demo_menu">
        <ul id="demo_menu_items">
          <?php
          foreach ($categories as $category) {
            $class = ( ! isset($class_inactive) ) ? 'current' : 'inactive';
            echo '<li class="' . $class . '"><a><span>' . $category->name . '</span></a></li>';
            $class_inactive = TRUE;
          }
          ?>
        </ul>
        <div class="clear"></div>
      </div>

      <div class="demo_body">
        <div class="ad_container">

          <div class="ad_display ad_display_iphone_top"
               style="display: <?php echo $iphone_ad_display; ?>">

            <?php if (variable_get('mopub_ad_option') == 'custom_ads'): ?>
            <!-- custom ad -->
            <img src="<?php echo $ad_url; ?>" width="229" height="59" id="demoAdvert"/>
            <?php else: ?>
            <!-- admob / ad server embed code -->
            <img src="<?php echo $path . '/images/demo/sample_ad_banner.png';?>" width="229px" height="59px" id="demoAdvert"/>
            <?php endif; ?>

          </div><!-- ad_display -->
        </div><!-- ad_container -->

        <?php if ( ! empty($nodes) ) : ?>
          <?php $demo_article_count = 0; ?>
          <?php foreach ($nodes as $node) : ?>
            <?php
            $demo_article_count++;
            // get actual pic from content for thumb   @todo resizing?
            if ( ! empty($image_field) && isset($node->{$image_field}['und']) ) {
              $thumbnail = file_create_url($node->{$image_field}['und'][0]['uri']);
            }
            ?>
            <div class="demo_article demo_article_<?php echo $demo_article_count; ?>">
              <?php if (!empty($thumbnail)) : ?>
              <img src="<?php echo $thumbnail; ?>" width="57px" height="57px"/>
              <?php else : ?>
              <img src="<?php echo $path . '/images/demo/default_thumbnail_75x75.png'; ?>" width="57px" height="57px"/>
              <?php endif; ?>
              <div class="imageview"></div>
              <div class="demo_post_content">
                <h3><a><?php echo drupal_substr($node->title, 0, 55); ?></a></h3>
                <div class="paragraph">
                  <?php if ( isset($node->body['und'][0]) ): ?>
                  <?php echo drupal_substr($node->body['und'][0]['value'], 0, 75); ?>
                  <?php endif; ?>...
                </div>
                <div class="meta">Published: <?php echo date('d F Y', $node->created) ?></div>
                <div class="clear"></div>
              </div><!-- demo_post_content -->
            </div>
			<?php unset($thumbnail); ?>
            <?php endforeach; ?>
          <?php endif; ?>
      </div><!-- demo_body -->

      <div class="demo_bottommenu">
        <ul id="tab_menu_items">
          <?php foreach ($tabs as $tab_name): ?>
          <li class="icon_<?php echo $tab_name ?>">
            <a><?php echo $tab_name ?></a>
          </li>
          <?php endforeach; ?>
        </ul>
      </div>

    </div><!-- viewer -->
  </div><!-- phone -->

</div><!-- mopub_demo -->