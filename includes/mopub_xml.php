<?php
/**
 * @file
 * Provides XML feeds for MoPublication
 */

/**
 * MoPublication Category list XML feed
 */
function mopublication_category_listing() {

  //get categories
  $categories = mopublication_get_terms('mopub_category_vocabulary');

  //include XML template
  include drupal_get_path('module', 'mopublication') . "/xml/category_listing.php";
}

/**
 * MoPublication Tag list XML feed
 */
function mopublication_tag_listing() {

  //get tags
  $tags = mopublication_get_terms('mopub_tag_vocabulary');

  //include XML template
  include drupal_get_path('module', 'mopublication') . "/xml/tag_listing.php";
}

/**
 * MoPublication Video Listing XML feed
 */
function mopublication_video_listing() {

  $video_node_type = variable_get('mopub_video_node_type');
  $video_field = variable_get('mopub_video_field');

  $query = db_select('node', 'n')
    ->fields('n', array('nid', 'title', 'created'))
    ->condition('status', 1)  // published
    ->condition('type', $video_node_type)
    ->orderBy('created', 'DESC') // most recent first
    ->range(0, 20)
    ->execute();
  $data = $query->fetchAllAssoc('nid');

  foreach ($data as $nid => $node_info) {
    $nodes[$nid] = node_load($nid);
  }

  //include XML template
  include drupal_get_path('module', 'mopublication') . '/xml/video_listing.php';
}

/**
 * MoPublication Audio Listing XML feed
 */
function mopublication_audio_listing() {

  $video_node_type = variable_get('mopub_audio_node_type');
  $video_field = variable_get('mopub_audio_field');

  $query = db_select('node', 'n')
    ->fields('n', array('nid', 'title', 'created'))
    ->condition('status', 1)  // published
    ->condition('type', $video_node_type)
    ->orderBy('created', 'DESC') // most recent first
    ->range(0, 20)
    ->execute();
  $data = $query->fetchAllAssoc('nid');

  foreach ($data as $nid => $node_info) {
    $nodes[$nid] = node_load($nid);
  }

  //include XML template
  include drupal_get_path('module', 'mopublication') . '/xml/audio_listing.php';
}

/**
 * MoPublication Config File XML feed
 */
function mopublication_config_file() {

  $tabs = mopublication_get_tabs();
  $categories = mopublication_get_terms('mopub_category_vocabulary');
  $tags = mopublication_get_terms('mopub_tag_vocabulary');

  $module_info = system_get_info('module', 'mopublication');

  //include XML template
  include drupal_get_path('module', 'mopublication') . '/xml/config_file.php';
}

/**
 * MoPublication Node listing
 */
function mopublication_node_listing() {

  $nodes = array();

  if ( ! empty($_GET['tid']) ) {
    // Get nodes for the taxonomy term
    $tid = (int) $_GET['tid'];
    $taxonomy_nodes = taxonomy_select_nodes($tid);
    foreach ($taxonomy_nodes as $nid) {
      $nodes[$nid] = node_load($nid);
    }
  }
  else {
    // General listing
    $query = db_select('node', 'n');

    $query->fields('n', array('nid', 'title', 'created'));
    $query->condition('status', 1);  // published
    $query->condition('promote', 1);

    if ( $node_type = variable_get('mopub_article_node_type') ) {
      $query->condition('type', $node_type);
    }

    if ( !empty($_GET['s']) ) {
      // Search
      $search_string = $_GET['s'];
      $query->condition('title', '%' . $search_string . '%', 'like');
    }

    $query->orderBy('created', 'DESC'); // most recent first
    $data = $query->execute()->fetchAllAssoc('nid');;

    foreach ($data as $nid => $node_info) {
      $nodes[$nid] = node_load($nid);
    }
  }

  // Handle paging in a simple fashion
  define("POSTS_PER_PAGE", 8);
  $page_number = 0;
  if ( ! empty($_GET['mopage']) ) {
    $page_number = (int)$_GET['mopage'];
  }
  $nodes = array_slice($nodes, $page_number * POSTS_PER_PAGE, POSTS_PER_PAGE, true);

  $image_field = variable_get('mopub_image_field');

  include drupal_get_path('module', 'mopublication') . '/xml/node_listing.php';
}

/**
 * MoPublication Node Detail
 */
function mopublication_node_detail() {

  if (isset($_GET['nid'])) {
    $nid = $_GET['nid'];
  }
  else {
    die('Node ID is missing.');
  }

  $node = node_load($nid);
  $comments = comment_get_thread($node, COMMENT_MODE_FLAT, 10);

  $image_field = variable_get('mopub_image_field');

  include drupal_get_path('module', 'mopublication') . '/xml/node_detail.php';
}
