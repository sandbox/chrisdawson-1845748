<?php
/**
 * @file
 * Contains web services provided by MoPublication
 */

/**
 *  Post a comment as anonymous user
 */
function mopublication_service_post_comment() {

  //get post data

  if ( empty($_POST['comment_post_ID']) || empty($_POST['comment_author']) ||
    empty($_POST['comment_author_email']) ||  empty($_POST['comment_content']) ) {
    echo json_encode(array('status' => 'error', 'message' => 'All fields are required.'));
    exit;
  }

  $nid = (int) check_plain($_POST['comment_post_ID']);

  if ( ! node_load($nid) ) {
    echo json_encode(array('status' => 'error', 'message' => 'The node does not exist.'));
    exit;
  }

  if ( ! user_access('post comments', drupal_anonymous_user()) ) {
    echo json_encode(array('status' => 'error', 'message' => 'Permission to post comments has been denied.'));
    exit;
  }

  $comment_body[LANGUAGE_NONE][0]['value'] = filter_xss($_POST['comment_content']);
  $comment_body[LANGUAGE_NONE][0]['format'] = 'filtered_html';

  $comment = array(
    'nid' => $nid,
    'cid' => 0,  // Comment ID (new comment)
    'pid' => 0,  // Parent ID (for threaded comments)
    'uid' => 0,  // anonymous
    'mail' => filter_xss($_POST['comment_author_email']),
    'name' => filter_xss($_POST['comment_author']),
    'is_anonymous' => 1,
    'language' => LANGUAGE_NONE,
    'subject' => '',
    'comment_body' => $comment_body,
  );
  if ( ! empty($_POST['comment_author_url']) ) {
    $comment['homepage'] = filter_xss($_POST['comment_author_url']);
  }

  $comment = (object) $comment;

  if ( ! $comment = comment_submit($comment) ) {
    echo json_encode(array('status' => 'error', 'message' => 'There was an error submitting the comment, please try again later.'));
    exit;
  }

  try {
    comment_save($comment);
  }
  catch (Exception $e) {
    echo json_encode(array('status' => 'error', 'message' => 'There was an error adding the comment, please try again later.'));
    exit;
  }

  echo json_encode(array('status' => 'success', 'message' => 'The comment has been added.'));
  exit;
}
