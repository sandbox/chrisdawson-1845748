<?php
/**
 * @file
 * Help for MoPublication.module
 */
 ?>

<h1>Using MoPublication</h1>

<p>We've done our best to make the process of configuring your app as simple and straightforward as possible.</p>

<p>Refer to the description next to each field for a more detailed explanation of what every setting does.
    The live preview will reflect your changes. Once you're happy with how your app looks and feels, all you need to do
    is submit it through the final tab, and select a package and payment option. We'll take care of the rest.</p>

<p>MoPublication has been designed to work best with the Seven theme.  If things don't look quite right, try changing
your admin theme to Seven (included with Drupal 7) while you set up your app.</p>

<p><b>A note on Taxonomy</b><br />
You will need set up a Taxonomy Vocabulary for MoPublication to use (unless you'd like to use an existing
Vocabulary).  For help setting up a Vocabulary, please see the
<?php echo l(t('Taxonomy help'), 'admin/help/taxonomy'); ?>.</p>

<p><b>Audio and Video tabs</b><br />
To use the Audio and Video tabs, you will need to set up custom content types with a File field for the audio
and video file respectively.  File types supported are MP3 (audio) and MP4 (video).</p>

<p>If you experience any problems that aren't addressed here, please browse our
<a href='http://support.mopublication.com' target='_blank'>FAQs</a> or
<a href='http://www.mopublication.com/contact/' target='_blank'>Contact us</a>. We'll be happy to assist you.</p>

<p>Enjoy your new app!</p>
