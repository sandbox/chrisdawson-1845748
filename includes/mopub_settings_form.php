<?php
/**
 * @file
 * MoPublication main configuration form
 */

require_once "mopub_validate.php";
require_once "mopub_settings_form_1.php";
require_once "mopub_settings_form_2.php";
require_once "mopub_settings_form_3.php";
require_once "mopub_settings_form_4.php";

/**
 * MoPublication settings FormAPI form
 */
function mopublication_settings_form($form, &$form_state) {

  $form['ajax_save_wrapper'] = array(
    '#type' => 'markup',
    '#prefix' => '<div id="ajax_save_wrapper">',
    '#suffix' => '</div>',
    '#markup' => '',
  );

  $form['ajax_save'] = array(
    '#type' => 'submit',
    '#ajax' => array(
      'callback' => 'mopublication_settings_form_save',
      'wrapper' => 'ajax_save_wrapper',
      'name' => 'ajax_save',
    ),
    '#value' => t('Save Changes'),
  );

  $form['vertical_tabs'] = array(
    '#type' => 'vertical_tabs',
  );

  $form['save_success_markup'] = array(
    '#markup' => '<div style="display: none;" class="mopub_save_success">Saved successfully.</div>',
  );

  $form = mopublication_settings_form_1($form);  // Customize
  $form = mopublication_settings_form_2($form);  // Content management
  $form = mopublication_settings_form_3($form);  // Third-party
  $form = mopublication_settings_form_4($form);  // Advanced

  $form['finished'] = array(
    '#type' => 'fieldset',
    '#title' => t('5. Finished!'),
    '#group' => 'vertical_tabs',
  );

  $form['finished']['finished_markup'] = array(
    '#markup' => '<div class="alldone">
        <h2 style="margin-bottom: 5px;">I\'m done! What happens next?</h2>
        <p>If you are 100% happy with how your app looks, go ahead and hit the submit button.</p>
        <ol>
            <li>You\'ll be taken to a subscription form where we\'ll capture your details</li>
            <li>Then you\'ll be asked to select a pricing plan before being directed to a secure payment area</li>
            <li>After your order has been finalized, we\'ll build your app and send you an email once it\'s live in
            the App Store</li>
        </ol>
        <p>Your app should be live on the App Store within 5-10 working days after submission. That\'s it!</p>
        <p>Still have questions? Take a look at our comprehensive
        <a href="http://support.mopublication.com" title="MoPublication FAQs" target="_blank">FAQ
        section</a> or <a href="http://www.mopublication.com/contact/"
        title="Contact MoPublication" target="_blank">Contact us</a>.
        </p>
    </div>',
  );

  $form['finished']['agree_terms'] = array(
    '#type' => 'checkbox',
    '#title' => t('I agree to the MoPublication Terms of Service.'),
  );

  $form['finished']['mopub_submitting_markup'] = array(
    '#markup' => '<div id="mopub_submitting_wait" style="display: none;">Submitting to MoPublication! Please wait...</div>',
  );

  $form['finished']['mopub_submit'] = array(
    '#type' => 'submit',
    '#ajax' => array(
      'callback' => 'mopublication_settings_form_submit_app',
      'name' => 'ajax_submit_app',
    ),
    '#value' => t('Submit my awesome app!'),
  );

  if (!empty($_POST) && form_get_errors()) {
    drupal_set_message(t('The settings have not been saved because of the errors.'), 'error');
  }

  $form['#submit'][] = 'mopublication_settings_form_submit';
  $form['#theme'] = 'system_settings_form';

  return $form;
}

/**
 * Ajax form save callback
 */
function mopublication_settings_form_save($form, $form_state) {
  $commands = array();
  $commands[] = ajax_command_invoke(NULL, 'mopub_save_success', array());
  return array('#type' => 'ajax', '#commands' => $commands);
}

/**
 * Ajax MoPub app submit callback
 *
 * This happens after the form has been submitted (saved)
 *
 * @param $form
 * @param $form_state
 */
function mopublication_settings_form_submit_app($form, $form_state) {

  //add server-side validation for required fields here @todo

  $commands = array();
  $commands[] = ajax_command_invoke(NULL, 'mopub_submit', array());

  // E-mail config file XML to MoPublication as a backup
  // We might want to move this into its own AJAX command so it doesn't slow down the submission
  mopublication_email_config_file();

  return array('#type' => 'ajax', '#commands' => $commands);
}

/**
 * MoPublication settings form submit
 *
 * Based on the system_settings_form_submit
 */
function mopublication_settings_form_submit($form, &$form_state) {

  foreach ($form_state['values'] as $field => $value) {

    //look for the managed_file files
    if (in_array($field, array(
      'mopub_topbar_logo', 'mopub_app_icon',
      'mopub_ad_iphone_splash_banner', 'mopub_ad_iphone_top_banner', 'mopub_ad_iphone_bottom_banner',
      'mopub_ad_ipad_splash_banner', 'mopub_ad_ipad_top_banner', 'mopub_ad_ipad_bottom_banner',
    ))) {
      //make file permanent
      $file = file_load($value);
      if (!empty($file)) {
        $file->status = FILE_STATUS_PERMANENT;
        file_save($file);
        file_usage_add($file, 'mopublication', 'variable:' . $field, $value);
      }
    }
  }

  /**
   * Based on system settings save
   */
  form_state_values_clean($form_state);
  foreach ($form_state['values'] as $key => $value) {
    if (is_array($value) && isset($form_state['values']['array_filter'])) {
      $value = array_keys(array_filter($value));
    }
    variable_set($key, $value);
  }

}

