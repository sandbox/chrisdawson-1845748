<?php
/**
 * @file
 * MoPublication module for Drupal
 *
 * http://www.mopublication.com/drupal
 */

require_once 'includes/mopub_theme.php';
require_once 'includes/helper_functions.php';

/**
 * Implements hook_help().
 */
function mopublication_help($path, $arg) {
  switch ($path) {
    case "admin/help#mopublication":
      ob_start();
      include 'includes/mopub_help.php';
      $html = ob_get_clean();
      return $html;
      break;
  }
}

/**
 * Implements hook_menu().
 */
function mopublication_menu() {

  $items = array();

  $items['admin/config/services/mopublication'] = array(
    'title' => 'MoPublication',
    'description' => 'Settings for MoPublication - set up iOS app for your website',
    'page callback' => 'mopublication_settings',
    'access arguments' => array('administer mopublication'),
    'file' => 'includes/mopub_settings_form.php',
  );

  $items['mopublication/xml/category-listing'] = array(
    'title' => 'MoPublication Category Listing',
    'type' => MENU_CALLBACK,
    'description' => 'Category Listing Feed for MoPublication',
    'page callback' => 'mopublication_category_listing',
    'access arguments' => array('access content'),
    'file' => 'includes/mopub_xml.php',
  );

  $items['mopublication/xml/tag-listing'] = array(
    'title' => 'MoPublication Tag Listing',
    'type' => MENU_CALLBACK,
    'description' => 'Tag Listing Feed for MoPublication',
    'page callback' => 'mopublication_tag_listing',
    'access arguments' => array('access content'),
    'file' => 'includes/mopub_xml.php',
  );

  $items['mopublication/xml/node-listing'] = array(
    'title' => 'MoPublication Node Listing',
    'type' => MENU_CALLBACK,
    'description' => 'Category List Feed for MoPublication',
    'page callback' => 'mopublication_node_listing',
    'access arguments' => array('access content'),
    'file' => 'includes/mopub_xml.php',
  );

  $items['mopublication/xml/node-detail'] = array(
    'title' => 'MoPublication Node Detail',
    'type' => MENU_CALLBACK,
    'description' => 'Display the contents of a single node in XML for MoPublication',
    'page callback' => 'mopublication_node_detail',
    'access arguments' => array('access content'),
    'file' => 'includes/mopub_xml.php',
  );

  $items['mopublication/xml/config-file'] = array(
    'title' => 'MoPublication Config File',
    'type' => MENU_CALLBACK,
    'description' => 'Generate the complete MoPublication config file in XML format.',
    'page callback' => 'mopublication_config_file',
    'access arguments' => array('access content'),
    'file' => 'includes/mopub_xml.php',
  );

  $items['mopublication/xml/audio-listing'] = array(
    'title' => 'MoPublication Audio Listing',
    'type' => MENU_CALLBACK,
    'description' => 'Audio Listing Feed for MoPublication',
    'page callback' => 'mopublication_audio_listing',
    'access arguments' => array('access content'),
    'file' => 'includes/mopub_xml.php',
  );

  $items['mopublication/xml/video-listing'] = array(
    'title' => 'MoPublication Video Listing',
    'type' => MENU_CALLBACK,
    'description' => 'Video Listing Feed for MoPublication',
    'page callback' => 'mopublication_video_listing',
    'access arguments' => array('access content'),
    'file' => 'includes/mopub_xml.php',
  );

  $items['mopublication/service/post-comment'] = array(
    'title' => 'MoPublication Post Comment Service',
    'type' => MENU_CALLBACK,
    'description' => 'Post a comment to MoPublication',
    'page callback' => 'mopublication_service_post_comment',
    'access arguments' => array('access content'),
    'file' => 'includes/mopub_service.php',
  );

  return $items;
}

/**
 *  Display the MoPublication Settings page
 */
function mopublication_settings() {

  drupal_add_js(drupal_get_path('module', 'mopublication') . '/js/jquery-1.8.2.min.js');
  drupal_add_js(drupal_get_path('module', 'mopublication') . '/js/jquery.miniColors.min.js');
  drupal_add_js(drupal_get_path('module', 'mopublication') . '/js/jquery-noconflict.js');
  drupal_add_js(drupal_get_path('module', 'mopublication') . '/js/mopub_settings.js');
  drupal_add_js(drupal_get_path('module', 'mopublication') . '/js/mopub_validation.js');
  drupal_add_js(drupal_get_path('module', 'mopublication') . '/js/mopub_interface.js');

  // Add base_url to MoPublication JS settings
  drupal_add_js(array('mopublication' => array('base_url' => $GLOBALS['base_url'])), 'setting');

  drupal_add_css(drupal_get_path('module', 'mopublication') . '/css/mopub.css');
  drupal_add_css(drupal_get_path('module', 'mopublication') . '/css/jquery.miniColors.css');

  $form = drupal_get_form('mopublication_settings_form');
  $html = drupal_render($form);

  $html .= mopublication_render_demo();

  return $html;
}

/**
 * Render the live iPhone app demo
 *
 * @return string
 */
function mopublication_render_demo() {

  $path = $GLOBALS['base_path'] . drupal_get_path('module', 'mopublication');

  drupal_add_css(drupal_get_path('module', 'mopublication') . '/css/mopub_demo.css');

  ob_start();
  include 'css/mopub_demo.php';
  include 'includes/mopub_demo.php';
  $html = ob_get_clean();

  return $html;
}
