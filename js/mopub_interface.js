/**
 * Interface JavaScript for MoPublication
 */
jQuery(window).resize(function() {
    var body_width = jQuery("body").width();
    if ( body_width < 1200 ) {
        jQuery('#mopublication-settings-form').removeClass('wide');
    }
    else
    {
        jQuery('#mopublication-settings-form').addClass('wide');
    }
});

jQuery(document).ready(function(){
    
    jQuery(window).resize();
    
    jQuery('#edit-mopub-app-name').focusout(function () {
        
        var appName = jQuery('#edit-mopub-app-name').val();

        jQuery('#appNameStatus').html(" Looking up app name...");
        jQuery('#edit-mopub-app-name').css("background", "#FFF").css("color", "#000");
        
        if(appName != '') { 
            
            checkAppName(appName);
            
        } else {
            
            jQuery('#appNameStatus').html(" An app name is required.");
            jQuery('#appNameStatus').removeClass();
            jQuery('#edit-mopub-app-name').css("background", "red").css("color", "#FFF");
            
        }
        
    });

});

function checkAppName(appName) {
    
    appName = encodeURI(appName);
    appName = appName.replace(/'/g, "\\'");
    
    if(appName == 'appName') {
        
        jQuery('#appNameStatus').html(" An app name is required.");
        jQuery('#appNameStatus').removeClass();
        jQuery('#edit-mopub-app-name').css("background", "red").css("color", "#FFF");
        
    } else {
        
        jQuery.getJSON("http://www.mopublication.com/service/app_name_service.php?&location="+document.URL+"&appname="+appName+"&callback=?",
            function(data){
                
                var objReturn = data;

                if (objReturn.appExists == 'yes') {

                    jQuery('#appNameStatus').html(" An app already exists with this name.");
                    jQuery('#appNameStatus').removeClass();
                    jQuery('#edit-mopub-app-name').css("background", "red").css("color", "#FFF");

                }

                if (objReturn.appExists == 'no') {

                    jQuery('#appNameStatus').html(" This app name is valid.");
                    jQuery('#appNameStatus').removeClass();
                    jQuery('#edit-mopub-app-name').css("background", "greenyellow");

                }

                if (objReturn.appExists == 'empty') {

                    jQuery('#appNameStatus').html(" An app name is required.");
                    jQuery('#appNameStatus').removeClass();
                    jQuery('#edit-mopub-app-name').css("background", "red").css("color", "#FFF");

                }

                if (objReturn.message) {

                    jQuery('#appNameStatus').html(objReturn.message);
                    jQuery('#appNameStatus').removeClass();
                    jQuery('#edit-mopub-app-name').css("background", "red").css("color", "#FFF");

                }
                
            });
        
    }
    
} 
