/**
 * JavaScript for MoPublication Settings
 */

jQuery(document).ready(function() {

    //use our own jQuery library for $
    (function($) {

        /**
         * Font styling for font selection list
         */
        $('#edit-mopub-type-font .form-item-mopub-type-font label').each(function() {
            $(this).css('font-family', $(this).html());
        })

        /**
         * Change the colours on the Live Demo in real time
         */
        $('input[id^="edit-mopub-"]').filter("[id$=-color]").miniColors({
            change: function(hex, rgb) {

                // update live preview

                switch (this.id)
                {
                    case 'edit-mopub-topbar-color':
                        $('.demo_topbar').css('background-color', hex);
                        break;

                    case 'edit-mopub-menu-bg-color':
                        $('.demo_menu').css('background-color', hex);
                        break;

                    case 'edit-mopub-menu-active-text-color':
                        $('.demo_menu .current a').css('color', hex);
                        break;

                    case 'edit-mopub-menu-active-bg-color':
                        $('#mopub_demo #demo_menu_items .current a').css('background-color', hex);
                        break;

                    case 'edit-mopub-menu-text-color':
                        $('.demo_menu .inactive a').css('color', hex);
                        break;

                    case 'edit-mopub-content-border-color':
                        $('.demo_article').css('border-color', hex);
                        break;

                    case 'edit-mopub-content-bg-color':
                        $('#mopub_demo .viewer').css('background-color', hex);
                        break;

                    case 'edit-mopub-type-title-color':
                        $('.demo_article h3 a').css('color', hex);
                        break;

                    case 'edit-mopub-type-meta-color':
                        $('.demo_article .meta').css('color', hex);
                        break;

                    case 'edit-mopub-type-link-color':
                        $('.demo_body p a').css('color', hex);
                        break;

                    case 'edit-mopub-type-body-color':
                        $('.demo_body').css('color', hex);
                        break;

                }
            }
        })

        /**
         *  Change the font on click
         */
        $('input[name=mopub_type_font]').change(function() {
            $('#mopub_demo .viewer').css('font-family', $(this).closest('div').find('label').css('font-family'));
        })

        /**
         *  Update the little tab selection in live preview
         */
        $('#mopub_tab_selection .draggable').mouseup(function() {
            mopub_tab_order_changed();
        })
        $('#mopub_tab_selection .draggable input').change(function() {
            mopub_tab_order_changed();
        })

        function mopub_tab_order_changed() {
            var tab_name, checkbox;
            var tab_list = [];
            //get new tab list from checkboxes
            $('#mopub_tab_selection tbody').find('tr.draggable').each(function(){
                tab_name = $(this).find('label').html().trim();
                var checkbox = $(this).find(':checkbox')[0];
                if (checkbox.checked) {
                    tab_list.push(tab_name.toLowerCase());
                }
            })
            mopub_update_tabs(tab_list);
        }

        function mopub_update_tabs(tab_list) {
            var j;
            var html = '';
            for (j in tab_list) {
                html += '<li class="icon_'+ tab_list[j] +'"><a>' + tab_list[j] +'</a></li>';
                if (tab_list.length > 5 && j >= 3) {
                    html += '<li class="icon_more"><a>more</a></li>';
                    break;
                }
            }
            $('#mopub_demo ul#tab_menu_items').html(html);
        }

        /**
         *  Update the countries list checkboxes when
         *  select the "Select all" checkbox
         */
        $('#select_all_countries').change (function () {
            //Check/uncheck all the list's checkboxes
            $('input[id^=edit-mopub-countries]').attr('checked', $(this).is(':checked'));
            //Remove the faded state
            $(this).removeClass('some_selected');
        });

        /**
         *  Update the state of Select box when the country check boxes get updated
         */
        $('input[id^=edit-mopub-countries]').change (function () {
            if ($('input[id^=edit-mopub-countries]:checked').length == 0) {
                $('#select_all_countries').removeClass('some_selected').attr('checked', false);
            } else if ($('input[id^=edit-mopub-countries]:not(:checked)').length == 0) {
                $('#select_all_countries').removeClass('some_selected').attr('checked', true);
            } else {
                $('#select_all_countries').addClass('some_selected').attr('checked', true);
            }
        });

        /**
         *  Set visibility of ad in the demo
         */
        $('#edit-mopub-ad-option').change(function() {
            if ($(this).val() == 'none') {
                $('#mopub_demo .ad_display_iphone_top').hide();
            } else {
                $('#mopub_demo .ad_display_iphone_top').show();
            }
        });


        /**
         * Update the layout style in app preview if the selection is changed
         */
        $('input[name=mopub_layout_option]').change(function() {
            $('#mopub_demo .viewer').removeClass('plain');
            $('#mopub_demo .viewer').removeClass('featured');
            $('#mopub_demo .viewer').removeClass('page');
            $('#mopub_demo .viewer').addClass($(this).attr("value"));
            $('#mopub_demo .viewer').addClass($(this).attr("value"));
            mopublication_setPageLayout();
        });

        /**
         *  Set the states of things on load
         */
        $('input[id^=edit-mopub-countries]').change();
        $('#edit-mopub-ad-option').change();
        mopublication_setPageLayout();
        mopub_tab_order_changed();

        function mopublication_setPageLayout() {
            var imageurl = 'url('+$('.demo_body .demo_article_1 img').attr('src')+')';
            $('.demo_body .demo_article_1 .imageview').css('background-image', imageurl);
            $('.demo_body .demo_article_1 .imageview').css('background-size', 'cover');
            $('.demo_body .demo_article_1 .imageview').css('background-position', 'center center');
            $('.demo_body .demo_article_1 .imageview').css('background-repeat', 'no-repeat');
            $('.demo_body .demo_article_1 .imageview').css('top', '0');
            $('.demo_body .demo_article_1 .imageview').css('right', '0');
            $('.demo_body .demo_article_1 .imageview').css('bottom', '0');
            $('.demo_body .demo_article_1 .imageview').css('left', '0');
        }

    }(jQueryMP))

});

/**
 * Save was successful
 *
 * Show a fading out confirmation message
 *
 * @param data
 * @return {Boolean}
 */
jQuery.fn.mopub_save_success = function(data) {
    jQuery('.mopub_save_success').show().delay(3000).fadeOut();
};


/**
 * Submit to MoPublication
 *
 * Steps:
 // 1. Save changes
 // 2. Do extra validation
 // 3. Show "Please wait" popup
 // 4. Post to callback getting the XML of config file
 // 5. Insert the XML into form element (new form)
 // 6. Submit new form.

 * @param data
 */
jQuery.fn.mopub_submit = function(data) {

    if ( !  mopub_validate_submission() ) {
        return false;
    }

    jQuery('#mopub_submitting_wait').show();  //show a message asking them to wait...

    jQuery.post(Drupal.settings.basePath + 'mopublication/xml/config-file', {}, function(response) {

        //console.log(response);
        //check here that response is good XML

        jQuery('body').append(
            '<form action="http://www.mopublication.com/members/signup/index/c/" name="mopub_app_submit"' +
                ' id="mopub_app_submit" method="post" style="display: none;">' +
                '<input type="hidden" name="site_url"    value="' + Drupal.settings.mopublication.base_url + '" />' +
                '<textarea name="config_file">' + response + '</textarea>' +
            '</form>');

        jQuery('#mopub_app_submit').submit();
    })

};
