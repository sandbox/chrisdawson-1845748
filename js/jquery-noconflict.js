/**
 *  Set up our own (newer version) of jQuery to be used
 *  in certain places where the Drupal-included jQuery is too old
 */

jQueryMP = jQuery;
jQuery.noConflict(true);
