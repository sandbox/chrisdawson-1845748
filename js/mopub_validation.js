/**
 *  @file
 *  Client side validation for MoPublication
 */

/**
 * Validate before we submit the configuration file to MoPublication
 */
function mopub_validate_submission()
{
    //create array of required fields which are missing
    var missing = [];

    var required = [
        ['#edit-mopub-app-name',               'Customize: General > App Name'],
        ['#edit-mopub-app-icon-name',          'Customize: General > Icon Name'],
        ['#edit-mopub-app-description',        'Customize: General > Description'],
        ['#edit-mopub-app-keywords',           'Customize: General > Keywords'],
        ['#edit-mopub-content-border-color',   'Customize: Content Area > Border Color'],
        ['#edit-mopub-content-bg-color',       'Customize: Content Area > Background Color'],
        ['#edit-mopub-menu-bg-color',          'Customize: Top Bar > Background Color'],
        ['#edit-mopub-menu-active-text-color', 'Customize: Menu > Text Color (active)'],
        ['#edit-mopub-menu-text-color',        'Customize: Menu > Text Color (inactive)'],
        ['#edit-mopub-type-title-color',       'Customize: Typography > Title Color'],
        ['#edit-mopub-type-meta-color',        'Customize: Typography > Meta Color'],
        ['#edit-mopub-type-link-color',        'Customize: Typography > Link Color'],
        ['#edit-mopub-type-body-color',        'Customize: Typography > Body Text Color']
    ];

    for (var field in required) {
        if ( mp_trim(jQuery(required[field][0]).val()) == '' ) {
            missing.push(required[field][1]);
        }
    }

    if ( missing.length > 0 )
    {
        var message = 'Hang on! Please fill in the required field(s):' + "\n\n";
        for (var j in missing) {
            message += missing[j] + "\n";
        }
        alert(message);
        return false;
    }

    // validate email
    if ( mp_trim(jQuery('#edit-mopub-contact-email').val()) != '' && ! mopub_validateEmail(jQuery('#edit-mopub-contact-email').val()) )
    {
        alert(Drupal.t('Your Contact Email address is not valid.'));
        return false;
    }

    // Check anonymous comments are allowed
    if ( jQuery('#edit-mopub-comment-option').val() == 'drupal' && ! jQuery('input[name=mopub_anonymous_comments_allowed]').val() )
    {
        alert(Drupal.t('Anonymous comments are not allowed. Please change your Comment option, or enable anonymous ' +
            'commenting in the Drupal user permissions.'));
        return false;
    }

    // Check audio type and field selected
    if ( jQuery('#edit-mopub-tabs-audio-tab-audio-checked').is(':checked') ) {
        if ( jQuery('#edit-mopub-audio-node-type').val() == '-' || jQuery('#edit-mopub-audio-field').val() == '-' ) {
            alert(Drupal.t('The Audio tab has been selected - please select the node type and File field for audio ' +
                'content.'));
            return false;
        }
    }

    // Check video type and field selected
    if ( jQuery('#edit-mopub-tabs-video-tab-video-checked').is(':checked') ) {
        if ( jQuery('#edit-mopub-video-node-type').val() == '-' || jQuery('#edit-mopub-video-field').val() == '-' ) {
            alert(Drupal.t('The Video tab has been selected - please select the node type and File field for ' +
                'video content.'));
            return false;
        }
    }

    // Advanced
    if ( jQuery('#edit-mopub-age-graphic-violence').val() != "none" || jQuery('#edit-mopub-age-graphic-violence').val() != "none" )
    {
        alert(Drupal.t("Your app may not contain Prolonged Graphic or Sadistic Realistic Violence or Graphic" +
            " Sexual Content and Nudity. It will be rejected by the App Store."));
        return false;
    }

    // Check that they have agreed to the Terms of Service
    if ( ! jQuery('#edit-agree-terms').is(':checked') )
    {
        alert(Drupal.t("Please agree to the Terms of Service."));
        return false;
    }

    //everything above has succeeded
    return true;
}

/**
 * Read here for why "validating e-mail addresses with regular expressions is probably a bad idea" :)
 * http://stackoverflow.com/questions/46155/validate-email-address-in-javascript
 *
 * @param email
 * @return {Boolean}
 */
function mopub_validateEmail(email)
{
    var regex = /\S+@\S+\.\S+/;
    return regex.test(email);
}

/**
 *  Javascript trim, ltrim, rtrim
 *  http://www.webtoolkit.info/
 *
 *  Using this method instead of jQuery .trim() because of IE 7/8 compatibility
 **/

function mp_trim(str, chars) {
    return mp_ltrim(mp_rtrim(str, chars), chars);
}

function mp_ltrim(str, chars) {
    chars = chars || "\\s";
    return str.replace(new RegExp("^[" + chars + "]+", "g"), "");
}

function mp_rtrim(str, chars) {
    chars = chars || "\\s";
    return str.replace(new RegExp("[" + chars + "]+$", "g"), "");
}
