
CONTENTS OF THIS FILE
---------------------

 * Intro to MoPublication
 * Requirements
 * Optional requirements
 * More Information


INTRO TO MOPUBLICATION
----------------------

MoPublication is the easiest and most cost effective way to create an iOS app for your website.

Just download and install the MoPublication module, and start customizing your iOS app. You can change colors, fonts,
icons, advertising options, and review all your changes through a live preview.

Once you're happy with how your app looks and feels, all you need to do is submit it through the module and select a
package and payment option. We'll take care of the rest.

For more information on MoPublication:

- View our Pricing Plans (http://www.mopublication.com/pricing/), or
- Take the Tour. (http://www.mopublication.com/tour/)

USING MOPUBLICATION.MODULE
--------------------------

You will need set up a Taxonomy Vocabulary for MoPublication to use (unless you'd like to use an existing Vocabulary).

Once you have set up your Vocabulary and assigned Terms to your content to its Terms, you'll be able to display
your content within the mobile app.

To use the Audio and Video tabs, you will need to set up custom content types with a File field for the audio
and video file respectively.  File types supported are MP3 (audio) and MP4 (video).

REQUIREMENTS
------------

- Drupal 7.x
- Taxonomy.module

OPTIONAL REQUIREMENTS
---------------------

- Comment.module


MORE INFORMATION
----------------

- For additional information, see the web site at http://www.mopublication.com

