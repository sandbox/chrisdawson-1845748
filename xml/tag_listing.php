<?php
/**
 * @file
 * XML template for MoPublication tag listing feed
 */

echo '<?xml version="1.0" encoding="UTF-8"?>' . "\n";
?>
<rss version="2.0">
  <channel>
    <title><![CDATA[MoPublication - Tag List]]></title>
    <description><![CDATA[List of the tags which should be displayed in the MoPublication app]]></description>
    <link><![CDATA[<?php echo $GLOBALS['base_url']; ?>]]></link>
    <lastBuildDate></lastBuildDate>
    <generator><![CDATA[MoPublication module for Drupal]]></generator>
    <?php foreach ($tags as $tag): ?>

    <item>
      <title><![CDATA[<?php echo $tag; ?>]]></title>
      <description></description>
      <author></author>
      <category></category>
      <pubDate></pubDate>
      <updated></updated>
      <guid></guid>
    </item>
    <?php endforeach; ?>

  </channel>
</rss>
