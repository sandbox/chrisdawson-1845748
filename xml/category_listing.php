<?php
/**
 * @file
 * XML template for MoPublication category listing feed
 */

echo '<?xml version="1.0" encoding="UTF-8"?>' . "\n";
?>
<rss version="2.0">
  <channel>
    <title><![CDATA[MoPublication - Category List]]></title>
    <description><![CDATA[List of the categories for the site which should be displayed in the
      MoPublication mobile app]]></description>
    <link><![CDATA[<?php echo $GLOBALS['base_url']; ?>]]></link>
    <lastBuildDate></lastBuildDate>
    <generator><![CDATA[MoPublication module for Drupal]]></generator>
    <?php foreach ($categories as $cat): ?>

      <item>
        <title><![CDATA[<?php echo $cat; ?>]]></title>
        <description></description>
        <author></author>
        <category></category>
        <pubDate></pubDate>
        <updated></updated>
        <guid></guid>
      </item>
    <?php endforeach; ?>

  </channel>
</rss>
