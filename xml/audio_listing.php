<?php
/**
 * @file
 * XML template for MoPublication audio listing feed
 */

echo '<?xml version="1.0" encoding="UTF-8"?>' . "\n";
?>
<rss version="2.0">
  <channel>
    <title><![CDATA[MoPublication Audio Listing]]></title>
    <description></description>
    <link><![CDATA[<?php echo $GLOBALS['base_url']; ?>]]></link>
    <generator>MoPublication module for Drupal</generator>

    <?php if (empty($nodes)) $nodes = array(); ?>
    <?php foreach ($nodes as $node): ?>

    <item>
      <title><![CDATA[<?php echo $node->title; ?>]]></title>
      <link><![CDATA[<?php echo $GLOBALS['base_url'] . '/node/' . $node->nid; ?>]]></link>
      <description><![CDATA[]]></description>
      <author><![CDATA[]]></author>
      <category><![CDATA[]]></category>
      <pubDate><![CDATA[<?php echo $node->created; ?>]]></pubDate>
      <a_headline><![CDATA[<?php echo $node->title; ?>]]></a_headline>
      <a_id><![CDATA[<?php echo $node->nid; ?>]]></a_id>
      <a_publish_date><![CDATA[<?php echo $node->created; ?>]]></a_publish_date>
      <a_author><![CDATA[]]></a_author>
      <a_thumbnail><![CDATA[]]></a_thumbnail>
      <?php if (isset($node->{$audio_field})): ?>
      <a_media_file><![CDATA[
          <?php echo file_create_url($node->{$audio_field}['und'][0]['uri']); ?>
      ]]></a_media_file>
      <?php endif; ?>

      <updated><![CDATA[<?php echo $node->changed; ?>]]></updated>
      <guid><![CDATA[<?php echo $GLOBALS['base_url'] . '/node/' . $node->nid; ?>]]></guid>
    </item>
    <?php endforeach; ?>

  </channel>
</rss>