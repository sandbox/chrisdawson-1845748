<?php
/**
 * @file
 * XML template for MoPublication node detail feed
 */

echo '<?xml version="1.0" encoding="UTF-8"?>' . "\n";
?>
<rss version="2.0">
  <channel>
    <title><![CDATA[MoPublication Node Detail]]></title>
    <description></description>
    <link><![CDATA[<?php echo $GLOBALS['base_url']; ?>]]></link>
    <generator>MoPublication module for Drupal</generator>
    <item>
      <title><![CDATA[<?php echo $node->title; ?>]]></title>
      <link><![CDATA[<?php echo $GLOBALS['base_url'] . '/node/' . $node->nid; ?>]]></link>
      <description><![CDATA[]]></description>
      <author><![CDATA[]]></author>
      <category><![CDATA[]]></category>
      <pubDate><![CDATA[<?php echo $node->created; ?>]]></pubDate>

      <a_id><![CDATA[<?php echo $node->nid; ?>]]></a_id>
      <a_publish_date><![CDATA[<?php echo $node->created; ?>]]></a_publish_date>
      <a_author><![CDATA[]]></a_author>
      <a_content><![CDATA[<?php echo $node->body['und'][0]['safe_value']; ?>]]></a_content>

      <?php if ( ! empty($image_field) && isset($node->{$image_field}['und']) ): ?>

      <a_images>
      <?php foreach ($node->{$image_field}['und'] as $image): ?>

        <a_image>
          <ai_thumbnail><![CDATA[<?php echo file_create_url($image['uri']); ?>]]></ai_thumbnail>
          <ai_resized><![CDATA[<?php echo file_create_url($image['uri']); ?>]]></ai_resized>
          <ai_caption><![CDATA[]]></ai_caption>
          <ai_source><![CDATA[]]></ai_source>
        </a_image>
        <?php endforeach; ?>

      </a_images>
      <?php endif; ?>

      <a_comment_count><![CDATA[<?php echo $node->comment_count ?>]]></a_comment_count>
      <?php if (false) { //@todo ?>
      <a_media>
        <a_media_item>
          <aa_filename><![CDATA[]]></aa_filename>
          <aa_media_description><![CDATA[]]></aa_media_description>
          <ai_media_resized><![CDATA[]]></ai_media_resized>
          <ai_media_thumbnail><![CDATA[]]></ai_media_thumbnail>
        </a_media_item>
      </a_media>
      <?php } ?>
      <?php if ( count($comments) ): ?>

      <a_comments>
        <?php foreach ($comments as $comment_id) :?>
        <?php $comment = comment_load($comment_id); ?>

          <a_comment>
            <a_comment_subject><![CDATA[<?php echo $comment->subject; ?>]]></a_comment_subject>
            <a_comment_by><![CDATA[<?php echo $comment->comment_author; @todo ?>]]></a_comment_by>
            <a_comment_date><![CDATA[<?php echo $comment->created; ?>]]></a_comment_date>
            <a_comment_content><![CDATA[<?php echo $comment->comment_body['und'][0]['safe_value']; ?>]]></a_comment_content>
          </a_comment>
        <?php endforeach; ?>

      </a_comments>
      <?php endif; ?>

      <updated><![CDATA[<?php echo $node->changed; ?>]]></updated>
      <guid><![CDATA[<?php echo $GLOBALS['base_url'] . '/node/' . $node->nid; ?>]]></guid>
    </item>
  </channel>
</rss>