<?php
/**
 * @file
 * XML template for MoPublication node listing feed
 */

echo '<?xml version="1.0" encoding="UTF-8"?>' . "\n";
?>
<rss version="2.0">
  <channel>
    <title><![CDATA[MoPublication Node Listing]]></title>
    <description></description>
    <link><![CDATA[<?php echo $GLOBALS['base_url']; ?>]]></link>
    <generator>MoPublication module for Drupal</generator>

    <?php foreach ($nodes as $node): ?>

      <item>
        <title><![CDATA[<?php echo $node->title; ?>]]></title>
        <link><![CDATA[<?php echo $GLOBALS['base_url'] . '/node/' . $node->nid; ?>]]></link>
        <description><![CDATA[]]></description>
        <author><![CDATA[]]></author>
        <category><![CDATA[]]></category>
        <pubDate><![CDATA[<?php echo $node->created; ?>]]></pubDate>
        <a_headline><![CDATA[<?php echo $node->title; ?>]]></a_headline>
        <a_id><![CDATA[<?php echo $node->nid; ?>]]></a_id>
        <a_publish_date><![CDATA[<?php echo $node->created; ?>]]></a_publish_date>
        <a_abstract><![CDATA[<?php echo drupal_substr(strip_tags($node->body['und'][0]['value']), 0, 200); ?>]]></a_abstract>
        <a_author><![CDATA[]]></a_author>
        <a_thumbnail><![CDATA[<?php
          if ( ! empty($image_field) && isset($node->{$image_field}['und'][0]) ) {
            echo image_style_url('mopub_thumbnail', $node->{$image_field}['und'][0]['uri']);
          }
          ?>]]></a_thumbnail>
        <?php if (!empty($node->comment_count)): ?>

        <a_comment_count><![CDATA[<?php echo $node->comment_count; ?>]]></a_comment_count>
        <?php endif; ?>

        <updated><![CDATA[<?php echo $node->changed; ?>]]></updated>
        <guid><![CDATA[<?php echo $GLOBALS['base_url'] . '/node/' . $node->nid; ?>]]></guid>
      </item>
      <?php endforeach; ?>

  </channel>
</rss>
