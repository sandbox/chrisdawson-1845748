<?php
/**
 * @file
 * XML template for MoPublication config file
 */

echo '<?xml version="1.0" encoding="UTF-8"?>' . "\n";
?>
<moPublication_config>
  <!-- Meta -->
  <cms><![CDATA[Drupal]]></cms>
  <config_plugin_version><![CDATA[<?php echo $module_info['version']; ?>]]></config_plugin_version>
  <app_site_url><![CDATA[<?php echo $GLOBALS['base_url']; ?>]]></app_site_url>
  <config_generation_timestamp><?php echo time(); ?></config_generation_timestamp>
  <amember_user_id><![CDATA[]]></amember_user_id>

  <!-- ==== Customize ==== -->

  <!-- General -->
  <app_display_name><![CDATA[<?php echo variable_get('mopub_app_name'); ?>]]></app_display_name>
  <app_icon_name><![CDATA[<?php echo variable_get('mopub_app_icon_name'); ?>]]></app_icon_name>
  <app_description><![CDATA[<?php echo variable_get('mopub_app_description'); ?>]]></app_description>
  <app_keywords><![CDATA[<?php echo str_replace(' ', '', variable_get('mopub_app_keywords')); ?>]]></app_keywords>

  <!-- Top bar -->
  <navbar_color><![CDATA[<?php echo variable_get('mopub_topbar_color'); ?>]]></navbar_color>
  <search_bar_color><![CDATA[<?php echo variable_get('mopub_topbar_color'); ?>]]></search_bar_color>

  <!-- Menu -->
  <sections_bar_color><![CDATA[<?php echo variable_get('mopub_menu_bg_color'); ?>]]></sections_bar_color>
  <sections_inactive_color><![CDATA[<?php echo variable_get('mopub_menu_text_color'); ?>]]></sections_inactive_color>
  <sections_active_color><![CDATA[<?php echo variable_get('mopub_menu_active_text_color'); ?>]]></sections_active_color>
  <section_button_back_color><![CDATA[<?php echo variable_get('mopub_menu_active_bg_color'); ?>]]></section_button_back_color>

  <!-- Layout options -->
  <listing_type><![CDATA[<?php echo variable_get('mopub_layout_option'); ?>]]></listing_type>

  <!-- Images -->
  <company_logo><![CDATA[<?php echo basename(mopublication_get_file_url('mopub_topbar_logo')); ?>]]></company_logo>
  <company_logo_full><![CDATA[<?php echo mopublication_get_file_url('mopub_topbar_logo'); ?>]]></company_logo_full>
  <app_icon><![CDATA[<?php echo mopublication_get_file_url('mopub_app_icon'); ?>]]></app_icon>

  <!-- Content area -->
  <listing_border_color><![CDATA[<?php echo variable_get('mopub_content_border_color'); ?>]]></listing_border_color>
  <listing_background_color><![CDATA[<?php echo variable_get('mopub_content_bg_color'); ?>]]></listing_background_color>

  <!-- Typography -->
  <standard_listing_headline_color><![CDATA[<?php echo variable_get('mopub_type_title_color'); ?>]]></standard_listing_headline_color>
  <article_view_headline_color><![CDATA[<?php echo variable_get('mopub_type_title_color'); ?>]]></article_view_headline_color>
  <article_view_subheadline_color><![CDATA[<?php echo variable_get('mopub_type_title_color'); ?>]]></article_view_subheadline_color>
  <standard_listing_pubdate_color><![CDATA[<?php echo variable_get('mopub_type_meta_color'); ?>]]></standard_listing_pubdate_color>
  <article_view_author_color><![CDATA[<?php echo variable_get('mopub_type_meta_color'); ?>]]></article_view_author_color>
  <article_view_publishdate_color><![CDATA[<?php echo variable_get('mopub_type_meta_color'); ?>]]></article_view_publishdate_color>
  <article_view_link_color><![CDATA[<?php echo variable_get('mopub_type_link_color'); ?>]]></article_view_link_color>
  <standard_listing_abstract_color><![CDATA[<?php echo variable_get('mopub_type_body_color'); ?>]]></standard_listing_abstract_color>
  <font><![CDATA[<?php echo variable_get('mopub_type_font'); ?>]]></font>

  <!-- ==== Content management ==== -->

  <tab_order><![CDATA[<?php echo implode('|', $tabs); ?>]]></tab_order>

  <!-- Contact screen -->
  <about_page_content><![CDATA[<?php echo nl2br(strip_tags(variable_get('mopub_content_about'))); ?>]]></about_page_content>
  <contact_page_email><![CDATA[<?php echo variable_get('mopub_contact_email'); ?>]]></contact_page_email>
  <contact_page_content><![CDATA[<?php echo nl2br(strip_tags(variable_get('mopub_content_contact'))); ?>]]></contact_page_content>

  <!-- Feeds -->
  <search_feed><![CDATA[<?php echo $GLOBALS['base_url'] . '/mopublication/xml/node-listing?q='; ?>]]></search_feed>
  <search_view_feed><![CDATA[<?php echo $GLOBALS['base_url'] . '/mopublication/xml/node-detail?nid='; ?>]]></search_view_feed>
  <search_results_per_page><![CDATA[8]]></search_results_per_page>

  <tab1_section_titles><![CDATA[<?php
    foreach ($categories as $tid => $term_name) {
      echo isset($j) ? '|' : ''; $j = TRUE;
      echo $term_name;
    }
    ?>]]></tab1_section_titles>
  <tab1_section_feeds><![CDATA[<?php
    foreach ($categories as $tid => $term_name) {
      echo isset($k) ? '|' : ''; $k = TRUE;
      echo $GLOBALS['base_url'] . '/mopublication/xml/node-listing?tid=' . $tid;
    }
    ?>]]></tab1_section_feeds>
  <tab1_article_view_feeds><![CDATA[<?php
    foreach ($categories as $tid => $term_name) {
      echo isset($l) ? '|' : ''; $l = TRUE;
      echo $GLOBALS['base_url'] . '/mopublication/xml/node-detail?nid=';  //missing node id
    }
    ?>]]></tab1_article_view_feeds>

  <audio_listing_feed><![CDATA[<?php echo $GLOBALS['base_url'] . '/mopublication/xml/audio-listing'; ?>]]></audio_listing_feed>
  <audio_view_feed><![CDATA[<?php echo $GLOBALS['base_url'] . '/mopublication/xml/node-detail?nid='; ?>]]></audio_view_feed>
  <video_listing_feed><![CDATA[<?php echo $GLOBALS['base_url'] . '/mopublication/xml/video-listing'; ?>]]></video_listing_feed>
  <video_view_feed><![CDATA[<?php echo $GLOBALS['base_url'] . '/mopublication/xml/node-detail?nid='; ?>]]></video_view_feed>
  <category_listing_feed><![CDATA[<?php echo $GLOBALS['base_url'] . '/mopublication/xml/category-listing'; ?>]]></category_listing_feed>
  <topic_listing_feed><![CDATA[<?php echo $GLOBALS['base_url'] . '/mopublication/xml/tag-listing'; ?>]]></topic_listing_feed>

  <!-- ==== Third party integration ==== -->

  <!-- Statistics -->
  <google_analytics_id><![CDATA[<?php echo variable_get('mopub_analytics_id'); ?>]]></google_analytics_id>

  <!-- Social sharing -->
  <addthis_account><![CDATA[<?php echo variable_get('mopub_sharing_option'); ?>]]></addthis_account>
  <addthis_app_id><![CDATA[<?php echo variable_get('mopub_sharing_option') == 'my_own' ? variable_get('mopub_addthis_app_id') : ''; ?>]]></addthis_app_id>
  <addthis_pub_id><![CDATA[<?php echo variable_get('mopub_sharing_option') == 'my_own' ? variable_get('mopub_addthis_publisher_id') : ''; ?>]]></addthis_pub_id>
  <addthis_username><![CDATA[<?php echo variable_get('mopub_sharing_option') == 'my_own' ? variable_get('mopub_addthis_username') : ''; ?>]]></addthis_username>
  <addthis_twitter_callback><![CDATA[<?php echo variable_get('mopub_sharing_option') == 'my_own' ? variable_get('mopub_addthis_twitter') : ''; ?>]]></addthis_twitter_callback>
  <addthis_share_url><![CDATA[<?php //echo $pagelink .$permalink_seperator . 'type=share&article_id='; ?>]]></addthis_share_url>

  <!-- Ad management -->
  <admob_publisher_id><![CDATA[<?php
    if (variable_get('mopub_ad_option') == 'admob') {
      echo variable_get('mopub_admob_id');
    }
    ?>]]></admob_publisher_id>

  <content_iphone_ad_top><![CDATA[<?php
    echo mopublication_render_ad( 'mopub_ad_iphone_top_banner',
                          'mopub_ad_iphone_top_url',
                          'mopub_ad_embed_iphone_top');
  ?>]]></content_iphone_ad_top>

  <content_iphone_ad_bottom><![CDATA[<?php
    echo mopublication_render_ad('mopub_ad_iphone_bottom_banner', 'mopub_ad_iphone_bottom_url', 'mopub_ad_embed_iphone_bottom');
    ?>]]></content_iphone_ad_bottom>

  <content_ipad_ad_top><![CDATA[<?php
    echo mopublication_render_ad('mopub_ad_ipad_top_banner', 'mopub_ad_ipad_top_url', 'mopub_ad_embed_ipad_top');
    ?>]]></content_ipad_ad_top>

  <content_ipad_ad_bottom><![CDATA[<?php
    echo mopublication_render_ad('mopub_ad_ipad_bottom_banner', 'mopub_ad_ipad_bottom_url', 'mopub_ad_embed_ipad_bottom');
    ?>]]></content_ipad_ad_bottom>

  <iphone_launch_ad><![CDATA[<?php
    echo mopublication_render_ad('mopub_ad_iphone_splash_banner', 'mopub_ad_iphone_splash_url', 'mopub_ad_embed_iphone_splash');
    ?>]]></iphone_launch_ad>

  <ipad_launch_ad><![CDATA[<?php
    if (variable_get('mopub_ad_option') == 'custom_ads') {

      $iphone_ad_image_splash = variable_get('mopub_ad_ipad_splash_banner');
      if (!empty($iphone_ad_image_splash)) {
        echo "<a href='" . variable_get('mopub_ad_ipad_splash_url') . "'><img src='" . $iphone_ad_image_splash . "' /></a>";
      }

    }
    elseif (variable_get('mopub_ad_option') == 'ad_server') {
      echo variable_get('mopub_ad_embed_ipad_splash');
    }
    ?>]]></ipad_launch_ad>

  <!-- ==== Advanced ==== -->

  <!-- Language -->
  <app_language><![CDATA[<?php echo variable_get('mopub_language'); ?>]]></app_language>

  <!-- Countries -->
  <app_countries><![CDATA[<?php echo variable_get('mopub_countries_option'); ?>]]></app_countries>
  <app_countries_select><![CDATA[<?php
    if (is_array($countries_select = variable_get('mopub_countries'))) {
      foreach ($countries_select as $key => $selected) {
        if ( ! $selected) unset($countries_select[$key]);
      }
      echo implode(';', array_keys($countries_select));
    }
    ?>]]></app_countries_select>

  <!-- Categories -->
  <app_category_primary><![CDATA[<?php echo variable_get('mopub_category_primary'); ?>]]></app_category_primary>
  <app_category_secondary><![CDATA[<?php echo variable_get('mopub_category_secondary'); ?>]]></app_category_secondary>

  <!-- Age restriction -->
  <app_age_fantasy_violence><![CDATA[<?php echo variable_get('mopub_age_fantasy_violence'); ?>]]></app_age_fantasy_violence>
  <app_age_realistic_violence><![CDATA[<?php echo variable_get('mopub_age_realistic_violence'); ?>]]></app_age_realistic_violence>
  <app_age_sexual><![CDATA[<?php echo variable_get('mopub_age_sexual'); ?>]]></app_age_sexual>
  <app_age_profanity><![CDATA[<?php echo variable_get('mopub_age_profanity'); ?>]]></app_age_profanity>
  <app_age_drug><![CDATA[<?php echo variable_get('mopub_age_drug'); ?>]]></app_age_drug>
  <app_age_mature><![CDATA[<?php echo variable_get('mopub_age_mature'); ?>]]></app_age_mature>
  <app_age_gambling><![CDATA[<?php echo variable_get('mopub_age_gambling'); ?>]]></app_age_gambling>
  <app_age_horror><![CDATA[<?php echo variable_get('mopub_age_horror'); ?>]]></app_age_horror>
  <app_age_graphic_violence><![CDATA[<?php echo variable_get('mopub_age_graphic_violence'); ?>]]></app_age_graphic_violence>
  <app_age_graphic_sexual><![CDATA[<?php echo variable_get('mopub_age_graphic_sexual'); ?>]]></app_age_graphic_sexual>

  <!-- Security -->
  <plugin_use_xml_protection><![CDATA[no]]></plugin_use_xml_protection>
  <plugin_salt><![CDATA[]]></plugin_salt>

  <!-- Comments -->
  <comment_url><![CDATA[<?php echo $GLOBALS['base_url'] . '/mopublication/service/post-comment'; ?>]]></comment_url>
  <comment_option><![CDATA[<?php echo variable_get('mopub_comment_option'); ?>]]></comment_option>
  <disqus_short_name><![CDATA[<?php echo variable_get('mopub_disqus_short_name'); ?>]]></disqus_short_name>
  <disqus_api_key><![CDATA[<?php echo variable_get('mopub_disqus_api_key'); ?>]]></disqus_api_key>
  <disqus_user_api_key><![CDATA[<?php echo variable_get('mopub_disqus_user_api_key'); ?>]]></disqus_user_api_key>

</moPublication_config>

<?php

function mopublication_render_ad($image_var, $url_var, $embed_var) {
  $ret = '';
  if (variable_get('mopub_ad_option') == 'custom_ads') {
    $image = mopublication_get_file_url($image_var);
    if ( ! empty($image) ) {
      $ret .= '<a href="' . variable_get($url_var, '#') . '"><img src="' . $image . '" /></a>';
    }
  }
  elseif (variable_get('mopub_ad_option') == 'ad_server') {
    $ret .= variable_get($embed_var);
  }
  return $ret;
}
